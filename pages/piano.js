import Head from 'next/head'

import Layout from "../components/Layout"

import PagePiano from "../components/PagePiano"

export default function(props) {
  console.log('db',props.db);

  return (
    <Layout noHeader={true}>
      <Head>
        <title>Piano chords</title>
        <meta name="description" content="See how chords are played with piano" />
        <meta name="keywords" content="piano, chords, visual" />
        <meta name="copyright" content="Jarkko Hiitoluoma 2018" />
        <meta name="author" content="Jarkko Hiitoluoma" />
      </Head>
      <PagePiano />
    </Layout>
  )

}