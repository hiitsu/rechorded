import Router from "next/router";
import App, {Container} from 'next/app'
import WithAnalytics from '../components/WithAnalytics'
import InMemoryDatabase from '../components/InMemoryDatabase'

class CustomApp extends App {

  static async getInitialProps ({ Component, router, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    return {pageProps}
  }

  constructor(props){
    super(props);

    this.state = {

    }
  }

  componentDidMount(){
    console.log('_app::componentDidMount',this.props.analytics);
    const db = new InMemoryDatabase();
    db.initialize()
      .then( (res) => {
        //this.setState({db});
        setTimeout( () =>  this.setState({db}),500);
      });
  }

  componentDidCatch (error, errorInfo) {
    //console.log('custom error', error);
    super.componentDidCatch(error, errorInfo)
  }

  render () {
    const {Component, pageProps} = this.props;
    return <Container>
      <Component { ...pageProps } db={this.state.db} />
    </Container>
  }
}

export default WithAnalytics("UA-125580378-1", Router)(CustomApp);