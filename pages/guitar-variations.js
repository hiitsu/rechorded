import Head from 'next/head'

import Layout from "../components/Layout"

import PageGuitarChordVariations from "../components/PageGuitarChordVariations";

export default function(props) {
  return <Layout noHeader={true}>
    <Head>
      <title>Guitar chords variations</title>
      <meta name="description" content="Look up different variations of guitar chords in several tunings"/>
      <meta name="keywords" content="Guitar chords, easy chords, difficult chords, tunings, explore chords"/>
      <meta name="copyright" content="Jarkko Hiitoluoma 2018"/>
      <meta name="author" content="Jarkko Hiitoluoma"/>
    </Head>
    <PageGuitarChordVariations db={props.db}/>
  </Layout>

}