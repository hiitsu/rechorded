import Head from 'next/head'

import Layout from "../components/Layout"

import PageGuitarBeginner from "../components/PageGuitarBeginner";

export default function(props) {
  console.log('db',props.db);

  return (
    <Layout noHeader={true}>
      <Head>
        <title>Explore beginner chords by difficulty</title>
        <meta name="description" content="Beginner guitar chords by difficulty" />
        <meta name="keywords" content="beginner guitar chords, easy chords" />
        <meta name="copyright" content="Jarkko Hiitoluoma 2018" />
        <meta name="author" content="Jarkko Hiitoluoma" />
      </Head>
      <PageGuitarBeginner db={props.db}/>
    </Layout>
  )

}