import Head from 'next/head'

import Layout from "../components/Layout"

import SectionHero from "../components/SectionHero"
import SectionSummary from "../components/SectionSummary"
import SectionFeatures from "../components/SectionFeatures"

export default function(props) {
  return (
    <Layout noHeader={true}>
      <Head>
      <title>Rechorded is a chords and scale exploration tool</title>
        <meta name="description" content="Rechorded is just another chord and music exploring tool. More than 18000 guitar chords in 4 different tunings" />
        <meta name="keywords" content="chords, guitar, piano, easy chords, beginner chords" />
        <meta name="copyright" content="Jarkko Hiitoluoma 2018" />
        <meta name="author" content="Jarkko Hiitoluoma" />
      </Head>
      <SectionHero />

      <SectionFeatures />
    </Layout>
  )
}