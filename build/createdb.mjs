import { ChordFinder } from './R.mjs'
import fs from 'fs'

const chordFinder = new ChordFinder();
chordFinder.findAll();

fs.writeFileSync("./static/chords.json", JSON.stringify(chordFinder.all, null, 2));

