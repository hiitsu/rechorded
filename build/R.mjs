export const SYMBOL_CLOSED			= 'X';
export const SYMBOL_OPEN			= 'F';
export const GUITAR_MAX_REACH		= 4;
export const GUITAR_MAX_FRETS		= 22;
export const TUNING_MAP = {
  'standard'		:	[4,9,2,7,11,4],
  'dropd'			:	[2,9,2,7,11,4],
  'openg'			:	[2,7,2,7,11,2],
  'dropc'			:	[0,7,0,5,9,2]
};
export const TUNINGS = Object.keys(TUNING_MAP);
export const DEFAULT_TUNING		= TUNINGS[0];
export const PIANO_WHITES			= [1,0,1,0,1,1,0,1,0,1,0,1];
export const WHITES				= [0,2,4,5,7,9,11];
export const BLACKS				= [1,3,6,8,10];
export const NOTES = ['C','C#','D','D#','E','F','F#','G','G#','A','A#','B'];
export const SCALE_MAP = {
  'dorian'		:[0, 2, 3, 5, 7, 9, 10],
  'lydian'		:[0, 2, 4, 6, 7, 9, 11],
  'mixolydian'	:[0, 2, 4, 5, 7, 9, 10],
  'ionian'		:[0, 2, 4, 5, 7, 9, 11],
  'aeolian'		:[0, 2, 3, 5, 7, 8, 10],
  'phrygian'		:[0, 1, 3, 5, 7, 8, 10],
  'locrian'		:[0, 1, 3, 5, 6, 8, 10]
  //,'harmonic minor':[0, 2, 3, 5, 7, 8, 11]
};
export const SCALES = Object.keys(SCALE_MAP);

export const CHORD_MAP = {
  '2'		: [0,2,4,7],
  '5'		: [0,7],
  '-5'	: [0,4,6],
  '6'		: [0,4,7,9],
  '6/9'	: [0,4,9,13],
  '7'		: [0,4,7,10],
  '9'		: [0,4,7,10,14],
  '11'	: [0,4,7,10,14,17],
  //		'13'	: [0,4,7,10,14,17,21],
  'maj'	: [0,4,7],
  'maj7'	: [0,4,7,11],
  'maj9'	: [0,4,7,11,14],
  'maj11'	: [0,4,7,11,14,17],
  //		'maj13'	: [0,4,7,11,14,17,21],
  'm'		: [0,3,7],
  'm6'	: [0,3,7,9],
  'm7'	: [0,3,7,10],
  'm7-5'	: [0,3,6,10],
  'm9'	: [0,3,7,10,14],
  'm11'	: [0,3,7,10,14,17],
  //		'm13'	: [0,3,7,10,14,17,21],
  'add9'	: [0,4,7,14],
  'madd9'	: [0,3,7,14],
  '6add9'	: [0,4,7,9,14],
  'm6add9': [0,3,7,9,14],
  '7b5'	: [0,4,6,10],
  '7#5'	: [0,4,8,10],
  '7b9'	: [0,4,7,10,13],
  '7#9'	: [0,4,7,10,15],
  'sus4'	: [0,5,7],
  'sus2'	: [0,2,7],
  '7sus4'	: [0,5,7,10],
  'dim'	: [0,3,6],
  'dim7'	: [0,3,6,9]
};
export const CHORDS = Object.keys(CHORD_MAP);
/*
  major				maj			0, 4, 7
  minor				min			0, 3, 7
  augmented			aug			0, 4, 8
  diminished			dim			0, 3, 6
  7th (dominant) 		dom7		0, 4, 7, 10
  major 7th			maj7		0, 4, 7, 11
  minor 7th			min7		0, 3, 7, 10
  suspended 4th		sus4		0, 5, 7
  suspended 2nd		sus2		0, 2, 7
  6th (major)			maj6		0, 4, 7, 9
  minor 6th			min6		0, 3, 7, 9
  9th (dominant)		dom9		0, 4, 7, 10, 14
  major 9th			maj9		0, 4, 7, 11, 14
  minor 9th			min9		0, 3, 7, 10, 14
  diminished 7th		dim7		0, 3, 6, 9
  add9				add9		0, 4, 7, 14
  minor 11th			min11		0, 7, 10, 14, 15, 17
  11th (dominant)		dom11		0, 7, 10, 14, 17
  13th(dominant)		dom13		0, 7, 10, 14, 16, 21
  minor 13th			min13		0, 7, 10, 14, 15, 21
  major 13th			maj13		0, 7, 11, 14, 16, 21
  7-5 (dominant)		dom7-5		0, 4, 6, 10
  7+5 (dominant)		dom7+5		0, 4, 8, 10
  major 7-5			maj7-5		0, 4, 6, 11
  major 7+5			maj7+5		0, 4, 8, 11
  minor major 7		minmaj7 	0, 3, 7, 11
  7-5-9 (dominant)	dom7-5-9	0, 4, 6, 10, 13
  7-5+9 (dominant)	dom7-5+9	0, 4, 6, 10, 15
  7+5-9 (dominant)	dom7+5-9	0, 4, 8, 10, 13
  7+5+9 (dominant)	dom7+5+9	0, 4, 8, 10, 15
*/

export const GENRE_MAP = {
  'jazz'		: ['7','maj7','m7'],
  'metal'		: ['maj', 'm', '7', 'm7', '5'],
  'reggae'	: ['maj', 'm', '7', 'm7', '5','maj7', 'add9', 'sus4', 'sus2','m7-5'],
  'latino'	: ['maj','m', '7', 'm7', 'maj7', '5', '6', '9', 'm6'],
  'pop'		: ['maj','m', '7','m7', 'maj7', '5', '6','sus4', 'add9', 'sus2']
};
export const GENRES = Object.keys(GENRE_MAP);
export function associatedGenresFor(chordType) {
  const a = [];
  GENRES.forEach(e => {
    if( GENRE_MAP[e].includes(chordType) )
      a.push(e);
  });
  return a;
};

export function noteNumber(s) {
  const octave = s.charAt(s.length-1);
  const literal = s.substring(0,s.length-1);
  const note = NOTES.indexOf(literal.toLocaleUpperCase());
  return octave*12+note;
}

export function randomColor() {
  return `#${(Math.random() * 0xFFFFFF << 0).toString(16)}`;
}

export function permutations(v, m) { //v1.0
  for(var p = -1, j, k, f, r, l = v.length, q = 1, i = l + 1; --i; q *= i);
  for(var x = [new Array(l), new Array(l), new Array(l), new Array(l)], j = q, k = l + 1, i = -1;
      ++i < l; x[2][i] = i, x[1][i] = x[0][i] = j /= --k);
  for(r = new Array(q); ++p < q;)
    for(r[p] = new Array(l), i = -1; ++i < l; !--x[1][i] && (x[1][i] = x[0][i],
      x[2][i] = (x[2][i] + 1) % l), r[p][i] = m ? x[3][i] : v[x[3][i]])
      for(x[3][i] = x[2][i], f = 0; !f; f = !f)
        for(j = i; j; x[3][--j] == x[2][i] && (x[3][i] = x[2][i] = (x[2][i] + 1) % l, f = 1));
  return r;
};

export function encode(num, at, value, mask) {
  const inverse = ~(mask << at);
  const encoded = (value << at);
  return ((num & inverse) | encoded);
}

export function decode(num, at, mask) {
  const n = mask << at;
  return (n & num) >>> at;
}

function go(lookup,s,a,k) {
  if (k == a.length) {
    lookup[s] = true;
  } else {
    for(let i=0; i < a[k].length;i++) {
      go(lookup,s + a[k][i], a, k + 1);
    }
  }
}

export function possibilities(a) {
  const o = {};
  go(o,'',a,0);
  return Object.keys(o);
}

export function transposeIgnoreOctave(n, semitones) {
  let r = n+semitones;
  if( r < 0 ) {
    r = 12 + r;
  }
  return r%12;
}

export function difference(n, m) {
  let d = Math.abs(m - n);
  if( d > 6 ) d = 6-(d%6);
  return d;
}

export function chord2Notes(root, type) {
  const a = [];
  const steps = CHORD_MAP[typeof type === 'string' ? type : CHORDS[type]];
  for( let m = 0; m < steps.length; m++ ){
    a.push(transposeIgnoreOctave(root,steps[m]));
  }
  return a;
}

export function scale2Notes(_root, type) {
  const a = [];
  const steps = SCALE_MAP[typeof type === 'string' ? type : SCALES[type]];
  const root = typeof _root === 'string' ? NOTES.indexOf(_root) : _root;
  for( let m = 0; m < steps.length; m++ ){
    a.push(transposeIgnoreOctave(root,steps[m]));
  }
  return a;
}

export class Chord {
  constructor(root, type) {
    if( typeof root === 'string' )
      this.root = NOTES.indexOf(root);
    else
      this.root = root || 0;

    if( typeof type === 'string' )
      this.type = CHORDS.indexOf(type);
    else
      this.type = type || 0;
  }

  hash() {
    return this.root*1000 + this.type;
  }

  transposeIgnoreOctave(d) {
    this.root = transposeIgnoreOctave(this.root,d);
  }

  noteMatch(a) {
    const steps = CHORD_MAP[CHORDS[this.type]];
    const found = {};
    for(let j=0; j < a.length;j++) {
      let flag = false;
      for(let i=0; i < steps.length;i++) {
        if( (this.root + steps[i])%12 === a[j] ) {
          found[a[j]] = true;
          flag = true;
        }
      }
      if( !flag )
        return false;
    }
    return Object.keys(found).length === steps.length;
  }

  noteCount() {
    return CHORD_MAP[CHORDS[this.type]].length;
  }

  hasNote(n) {
    const steps = CHORD_MAP[CHORDS[this.type]];
    let m;
    for(m = 0; m < steps.length; m++ ){
      if( (this.root + steps[m]) %12 === n%12 )
        return true;
    }
    return false;
  }

  notesIgnoreOctave() {
    return chord2Notes(this.root,this.type);
  }

  scales() {
    const a = [];
    const self = this;
    NOTES.forEach((root, i) => {
      SCALES.forEach((type, j) => {
        const scaleNotes = scale2Notes(root,type);
        const chordNotes = self.notesIgnoreOctave();
        let flag = true;
        chordNotes.forEach(n => {
          if( !scaleNotes.includes(n) )
            flag = false;
        });
        if( flag )
          a.push([i,j]);
      });
    });
    return a;
  }

  genres() {
    const a = [];
    const self = this;
    GENRES.forEach(genre => {
      const types = GENRE_MAP[genre];
      if( types.includes(CHORDS[self.type]) )
        a.push(genre);
    });
    return a;
  }
}

export class GuitarChord extends Chord {
  constructor(root, type, barre, s, tuning) {
    super(root,type);
    this.s = s || 'XXXXXX';
    this.barre = parseInt(barre,10) || 0;
    this.tuning = tuning || DEFAULT_TUNING;
    return this;
  }

  toJSON(){
    return {
      root:this.root,
      type:this.type,
      s:this.s,
      barre:this.barre,
      tuning:this.tuning,
      fingerCount:this.fingersUsed(),
      difficulty:this.difficulty(),
      strummable:this.isStrummable()
    }
  }

  difficulty() {
    const a = this.barre == 0 ? 0 : 3;
    const b = this.fingersUsed();
    const c = this.playability();
    const d = this.fingerPlacementDifficulty();
    return parseInt(a+b+c+d,10);
  }

  fingerPlacementDifficulty() {
    let sum = 0;
    let mul = 1;
    if( this.barre )
      mul = 1.5;
    const a = this.s.match(/3/g);
    const b = this.s.match(/2/g);
    const c = this.s.match(/1/g);
    if( a )
      sum = sum + a.length*0.75;
    if( b )
      sum = sum + b.length*0.3;
    if( c )
      sum = sum + c.length*0.2;
    return sum*mul;
  }

  playability() {
    if( /^[^X]{6}$/g.test(this.s) )
      return 0;
    if( /^X[^X]{5}$/g.test(this.s) || /^XX[^X]{4}$/g.test(this.s) )
      return 1;
    if( /^X[^X]{4}X$/g.test(this.s) || /^[^X]{1}X[^X]{3}X$/g.test(this.s) )
      return 2.5;
    if( /^XXX[^X]{3}$/g.test(this.s) )
      return 1.5;
    if( /^[^X]{5}X$/g.test(this.s) )
      return 1.5;
    if( /^[^X]{1}X[^X]{4}$/g.test(this.s) || /^XX[^X]{3}X$/g.test(this.s) )
      return 2;
    return 0;
  }

  lowestStrumNoteIndex() {
    const i = this.s.indexOf(SYMBOL_CLOSED);
    let k = i+1;
    if( i === 5 || i === -1 )
      k = 0;
    return k;
  }

  lowestStrumNote() {
    const k = this.lowestStrumNoteIndex();
    const tuning = TUNING_MAP[this.tuning];
    return (tuning[k]+this.barre+parseInt(c,10))%12;
  }

  lowestOpenNote() {
    const tuning = TUNING_MAP[this.tuning];
    for(let i=0; i < 6; i++) {
      const c = this.s.charAt(i);
      if( c !== SYMBOL_CLOSED ) {
        return (tuning[i]+this.barre+parseInt(c,10))%12;
      }
    }
    return null;
  }

  fingersUsed() {
    let count = 0;
    for(let i=0; i < 6; i++) {
      const c = this.s.charAt(i);
      if( c !== SYMBOL_CLOSED && c !== SYMBOL_OPEN && c != 0 )
        count++;
    }
    if( this.barre !== 0 )
      count++;
    return count;
  }

  fretSpanAverage() {
    const pattern =/^[1234]$/g;
    let sum = 0;
    for(let i=0; i <= 5; i++) {
      if( pattern.test(this.s.charAt(i)) )
        sum += parseInt(this.s.charAt(i),10);
    }
    return sum/4;
  }

  isStrummable() {
    for(let i=2; i <= 5; i++) {
      if( this.s.charAt(i) === SYMBOL_CLOSED )
        return false;
    }
    if( this.lowestOpenNote())
      return true;
  }

  closedCount() {
    const m = this.s.match(/X/g);
    return m ? m.length : 0;
  }

  isValid() {
    const tuning = TUNING_MAP[this.tuning];
    if( this.lowestOpenNote() !== this.root )
      return false;
    if( this.fingersUsed() > 4 )
      return false;
    const count3 = this.s.match(/3/g);
    const count2 = this.s.match(/2/g);
    const count1 = this.s.match(/1/g);
    if( this.barre ) {
      if( count3 && count3.length >= 3 )
        return false;
    }
    if( !this.barre && count1 && count1.length == 2 && count3 && count3.length == 2 )
      return false;

    const found = [];
    for(let i=0; i < 6; i++) {
      const p = this.s.charAt(i);
      if( p === SYMBOL_CLOSED )
        continue;
      else if( p === SYMBOL_OPEN ) {
        if( !this.hasNote(tuning[i]) )
          return false;
        found.push(tuning[i]);
      }
      const n = transposeIgnoreOctave(tuning[i],this.barre+parseInt(p,10));
      if( !this.hasNote(n) )
        return false;
      found.push(n);
    }
    const result = this.noteMatch(found);
    return result;
  }
}

class Fretboard {
  constructor(_tuning) {
    this.tuning = _tuning;
    return this;
  }

  positions(notes, barreFret) {
    const c = [];
    for(let k=0; k < this.tuning.length; k++) {
      const b = [];
      if( k <= 1 || k >= 5 )
        b.push(SYMBOL_CLOSED);
      if( notes.includes(this.tuning[k]+barreFret) ) {
        if( barreFret === 0 )
          b.push(SYMBOL_OPEN);
        else
          b.push(0);
      }
      for(let j=0; j < GUITAR_MAX_REACH; j++) {
        if( notes.includes((this.tuning[k]+barreFret+j)%12) ) {
          b.push(j);
        }
      }
      c.push(b);
    }
    return c;
  }

  find(notes) {
    const o = {};
    for(let i=0; i < GUITAR_MAX_FRETS - GUITAR_MAX_REACH; i++) {
      o[i] = possibilities(this.positions(notes,i));
    }
    return o;
  }
}

export class ChordFinder {
  constructor() {
    this.all = [];
  }

  findAll() {
    TUNINGS.forEach(tuning => {
      NOTES.forEach(root => {
        CHORDS.forEach(type => {
          const chord = new Chord(root,type);
          if( chord.noteCount() <= 6 ) {
            const fretboard = new Fretboard(TUNING_MAP[tuning]);
            const o = fretboard.find(chord.notesIgnoreOctave());
            Object.keys(o).forEach(barre => {
              o[barre].forEach(s => {
                const guitarChord = new GuitarChord(root,type,barre,s,tuning);
                if( guitarChord.isValid() ) {
                  this.all.push(guitarChord);
                }
              });
            });
          }
        });
      });
    });
  }
}
