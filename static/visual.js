import * as R from './constants.js';

const SELECTED_CHORD_COLOR = "rgb(203, 11, 33)";
const SELECTED_SCALE_COLOR = "rgb(11, 222, 33)";
const FONT_FAMILY = "Arial";

const TEXT_SHADOW = {
  color:"rgb(0, 0, 0)",
  ox:2.0,
  oy:2.0,
  blur:1
};

function font(c,h) {
  c.font = `${h}px ${FONT_FAMILY}`;
}

function fillRect(c,x,y,w,h,radius){
  fillRectR(c,x-w/2,y-h/2,w,h,radius)
}

function fillRectR(c,x,y,w,h,radius){
  const r = radius || 5;
  c.beginPath();
  c.moveTo(x + r, y);
  c.lineTo(x + w - r, y);
  c.quadraticCurveTo(x + w, y, x + w, y + r);
  c.lineTo(x + w, y + h - r);
  c.quadraticCurveTo(x + w, y + h, x + w - r, y + h);
  c.lineTo(x + r, y + h);
  c.quadraticCurveTo(x, y + h, x, y + h - r);
  c.lineTo(x, y + r);
  c.quadraticCurveTo(x, y, x + r, y);
  c.closePath();
  c.fill();
}

function background(c,w,h){
  c.save();
  c.beginPath();
  c.moveTo(w, h);
  c.lineTo(0, h);
  c.lineTo(0, 0);
  c.lineTo(w, 0);
  c.lineTo(w, h);
  c.closePath();
  const gradient = c.createRadialGradient(w/2.0, h/2, 0.0, w/2,h/2, w/2-w/8);
  gradient.addColorStop(0.00, "rgb(0, 0, 0)");
  gradient.addColorStop(1.00, "rgb(36,36,36)");
  c.fillStyle = gradient;
  c.fill();
  c.restore();
}
function transparentCircle(c,x,y,r,fillStyle,a) {
  c.save();
  c.translate(x,y);
  c.globalAlpha = (a ? a : 1);
  c.fillStyle = fillStyle || 'blue';
  c.beginPath();
  c.arc(0,0,r, 0, Math.PI * 2, true );
  c.closePath();
  c.fill();
  c.restore();
}
function guitarDarkBrown(c,x,y,w,h) {
  c.save();
  //c.globalAlpha = 1.0;
  c.translate(x, y-5);
  /*c.shadowColor = "rgba(0, 0, 0, 0.75)";
  c.shadowOffsetX = 0.0;
  c.shadowOffsetY = 2.0;
  c.shadowBlur = 4.0;
  c.fillStyle = "rgb(47, 23, 17)";
  c.fillRect(0,0,w,h);*/
  c.globalAlpha = 1;
  const g = c.createLinearGradient(w,0,w,h);
  g.addColorStop(0.00, "rgb(148, 148, 148)");
  //g.addColorStop(0.28, "rgb(155, 77, 33)");
  //g.addColorStop(0.74, "rgb(155, 77, 33)");
  g.addColorStop(1.00, "rgb(34, 34, 34)");
  c.fillStyle = g;
  c.fillRect(0,0,w,h+10);
  //c.fill();
  c.globalAlpha = 1.0;
  c.restore();
}
function line(c,x,y,len,angle,fatness,fillStyle,strokeStyle,shadow){
  c.save();
  c.translate(x,y);
  c.rotate(angle);
  if( shadow ) {
    c.shadowColor = shadow.color;
    c.shadowOffsetX = shadow.ox;
    c.shadowOffsetY = shadow.oy;
    c.shadowBlur = shadow.blur;
  }
  c.beginPath();
  const a = fatness/2;
  c.moveTo(-a,0);
  c.lineTo(a,0);
  c.lineTo(a,len);
  c.lineTo(-a,len);
  c.lineTo(-a,0);
  c.closePath();
  if( fillStyle ) {
    c.fillStyle = fillStyle;
    c.fill();
  }
  if( strokeStyle ) {
    c.strokeStyle = strokeStyle;
    c.stroke();
  }
  c.restore();
}
function guitarFret(c,x,y,len,angle,fatness) {
  const g = c.createLinearGradient(-fatness/2,len,fatness/2,len);
  g.addColorStop(0.00, "#533");
  g.addColorStop(0.5, "#fee");
  g.addColorStop(1.00, "#a88");
  line(c,x,y,len,angle,fatness,g,null,{
    color:"rgba(0, 0, 0, 0.5)",
    ox:2.0,
    oy:0.0,
    blur:2.0
  });

}
function guitarString(c,x,y,len,angle,fatness,shadowSize) {
  const g = c.createLinearGradient(-fatness/2,len,fatness/2,len);
  g.addColorStop(0.00, "rgb(149,149,149)");
  g.addColorStop(0.5, "#fff");
  g.addColorStop(1.00, "rgb(149,149,149)");
  line(c,x,y,len,angle,fatness,"white",null,{
    color:"rgba(0, 0, 0, 0.5)",
    ox:0.0,
    oy:4,
    blur:2.0
  });
  /*line(c,x,y,len,angle,1+fatness/2,g,null,{
    color:"rgba(0, 0, 0, 0.5)",
    ox:0.0,
    oy:4,
    blur:2.0
  });*/
}
function ellipse(c, x, y, w, h,fillStyle,strokeStyle) {
  c.save();
  const kappa = .5522848,
    ox = (w / 2) * kappa, // control point offset horizontal
    oy = (h / 2) * kappa, // control point offset vertical
    xe = x + w,           // x-end
    ye = y + h,           // y-end
    xm = x + w / 2,       // x-middle
    ym = y + h / 2;       // y-middle

  c.beginPath();
  c.moveTo(x, ym);
  c.bezierCurveTo(x, ym - oy, xm - ox, y, xm, y);
  c.bezierCurveTo(xm + ox, y, xe, ym - oy, xe, ym);
  c.bezierCurveTo(xe, ym + oy, xm + ox, ye, xm, ye);
  c.bezierCurveTo(xm - ox, ye, x, ym + oy, x, ym);
  c.closePath();
  if( fillStyle ) {
    c.fillStyle = fillStyle;
    c.fill();
  }
  if( strokeStyle ) {
    c.strokeStyle = strokeStyle;
    c.stroke();
  }
  c.restore();
}
function metalCircle(c,x,y,r) {
  c.save();
  const g = c.createRadialGradient(x,y,0,x,y,r);
  g.addColorStop(0.00, "rgb(203, 203, 203)");
  g.addColorStop(0.64, "rgb(179, 179, 179)");
  g.addColorStop(1.00, "rgb(152, 152, 152)");
  c.fillStyle = g;
  c.beginPath();
  c.arc(x,y,r, 0, Math.PI * 2, true );
  c.closePath();
  c.fill();
  c.restore();
}
function key(c,x,y,w,h,fill,stroke) {
  c.save();
  c.translate(x,y);
  //c.shadowColor = "rgba(0, 0, 0, 0.8)";
  //c.shadowOffsetX = 2.0;
  //c.shadowOffsetY = 2.0;
  //c.shadowBlur = 4.0;
  c.beginPath();
  c.moveTo(0,0);
  c.lineTo(0,h);
  c.lineTo(w, h);
  c.lineTo(w,0);
  c.lineTo(0,0);
  c.closePath();

  if( fill ) {
    c.fillStyle = fill || 'orange';
    c.fill();
  }
  if( stroke ) {
    c.strokeStyle = stroke || 'blue';
    c.stroke();
  }
  c.restore();
}
function blackFill(c,x,y,w,h) {
  const g = c.createLinearGradient(w/2, 0, w/2, h);
  g.addColorStop(0.33, "rgb(76, 76, 76)");
  g.addColorStop(1.00, "rgb(0, 0, 0)");
  return g;
}
function whiteFill(c,x,y,w,h) {
  const g = c.createLinearGradient(w/2, h, w/2, 0);
  g.addColorStop(0.77, "rgb(255, 255, 255)");
  g.addColorStop(1.00, "rgb(178, 178, 178)");
  return g;
}

function letterBall(c,x,y,r,ch,bgFill,textFill,shadow) {
  c.save();
  c.translate(x,y);
  c.beginPath();
  c.arc(0,0,r, 0, Math.PI * 2, true );
  c.closePath();
  c.fillStyle = bgFill || 'black';
  c.fill();
  c.restore();
  centeredText(c,x,y,r*2,r*2,ch,textFill || 'white',shadow);
}
function centeredText(c,x,y,w,h,t,fillStyle,shadow) {
  c.save();
  c.translate(x,y);
  if( shadow ) {
    c.shadowColor = shadow.color;
    c.shadowOffsetX = shadow.ox;
    c.shadowOffsetY = shadow.oy;
    c.shadowBlur = shadow.blur;
  }
  if( !fillStyle ) {
    const g = c.createLinearGradient(w,0,w,h);
    g.addColorStop(0.00, "rgb(127, 127, 127)");
    g.addColorStop(1.00, "rgb(255, 255, 255)");
    c.fillStyle = g;
  } else c.fillStyle = fillStyle;
  c.textAlign = 'center';
  const fontHeight = parseInt(h,10);
  font(c,fontHeight);
  c.fillText(t,0,(h/2)*0.7,w);
  c.restore();
}
function letterBox(c,x,y,w,h,ch,shadow) {
  c.save();
  c.translate(x,y);
  c.fillStyle = 'black';
  c.fillRect(-w/2,-h/2,w,h);
  c.restore();
  centeredText(c,x,y,w,h,ch,'white',shadow);
}

function gloss(c,w,h){
  c.save();
  c.globalAlpha = 0.10;
  c.beginPath();
  c.moveTo(0, 0);
  c.lineTo(0, h/4);
  c.lineTo(w, h/3);
  c.lineTo(w, 0);
  c.lineTo(0, 0);
  c.closePath();
  c.fillStyle = "rgb(255, 255, 255)";
  c.fill();
  c.globalAlpha = 1.0;
  c.restore();
}

function label(c,x,y,w,h,text,bgFill,textfill){
  fillRect(c,x,y,w,h,20,'black');
  centeredText(c,x,y,w,h,text,textfill);
}

class Instrument {
  constructor() {
    this.chord = [0,'maj'];
    this.scale = [null,null];
  }

  setChord(root, type) {
    this.chord[0] = typeof root === 'string' ? R.NOTES.indexOf(root) : root;
    this.chord[1] = typeof type === 'string' ? type : R.CHORDS[type];
  }

  setScale(root, type) {
    this.scale[0] = root;
    this.scale[1] = type;
  }

  chordText() {
    const root = typeof this.chord[0] !== 'string' ? R.NOTES[this.chord[0]] : this.chord[0];
    const type = typeof this.chord[1] !== 'string' ? R.CHORDS[this.chord[1]] : this.chord[1];
    return root + type;
  }

  scaleText() {
    const root = typeof this.scale[0] !== 'string' ? R.NOTES[this.scale[0]] : this.scale[0];
    const type = typeof this.scale[1] !== 'string' ? R.SCALES[this.scale[1]] : this.scale[1];
    return `${root}-${type}`;
  }
}

export class Guitar extends Instrument {

  constructor() {
    super();
    this.setTuning();
    this.dots = [2,5,9];
    this.doubledots = [7];
    this.frets = 7;
    this.strings = 6;
    this.s = 'X30210';
    this.barre = 0;
  }

  setTuning(tuningName) {
    this.tuning = tuningName ?
      R.TUNING_MAP[tuningName].slice(0).reverse() :
      R.TUNING_MAP[R.DEFAULT_TUNING].slice(0).reverse();
  }

  setChord(root, type, barre, s) {
    this.chord[0] = this.chord[0];
    this.chord[1] = this.chord[1];
    this.barre = barre || 0;
    this.s = s;
  }

  calculateStep(w, h) {
    const width = w/(this.frets-1);
    const height = h/(this.strings-1);
    return { width, height};
  }

  drawFrets(c, x, y, w, h) {
    const len = this.frets;
    const step = this.calculateStep(w,h);
    for(let j=0; j < len;j++ ) {
      const xpos = x+step.width*j;
      guitarFret(c,xpos,y-4,h+8,0,step.width*0.08);
    }
  }

  drawStrings(c, x, y, w, h) {
    const len = this.strings;
    const step = this.calculateStep(w,h);
    const fatnessBase = 1;
    for(let j=0; j < len;j++ ) {
      const ypos = y+step.height* j;
      const fatness = j/2+fatnessBase;
      guitarString(c,x,ypos,w,-Math.PI/2,fatness,2);
    }
  }

  drawDots(c, x, y, w, h) {
    const len = this.dots.length;
    const step = this.calculateStep( w,h);
    for(var j=0; j < len;j++ ) {
      let pos = this.dots[j];
      let xpos = x+step.width*pos+ step.width/2;
      //if( pos < this.barre || pos >= this.barre+this.frets ) break;
      metalCircle(c,xpos,y+2.5*step.height,step.height/4);
    }
    for(j=0; j < this.doubledots.length;j++ ) {
      let pos = this.doubledots[j];
      let xpos = x+step.width*pos;
      if( pos < this.barre || pos >= this.barre+this.frets )
        break;
      metalCircle(c,xpos,2.5*step.height, step.height/4 );
      metalCircle(c,xpos,4.5*step.height, step.height/4 );
    }
  }

  draw(c) {
    const w = c.canvas.width;
    const h = c.canvas.height;
    background(c,w,h);

    const smaller = w > h ? h : w;
    const margin = smaller/20;
    const fx = margin*2;
    const fy = margin;
    const fw = w-margin*3;
    const fh = h-margin*2;
    const fingers = this.s;
    const barre = this.barre;
    const step = this.calculateStep(fw,fh);
    guitarDarkBrown(c,fx,fy,fw,fh);
    for(var j=0; j < this.frets-1;j++ ) {
      const num = barre === 0 ? (j+1) : barre+j;
      letterBall(c,fx+(j+0.5)*step.width,margin/2,margin/2,`${num}`,"rgb(36,36,36)","white");
    }
    for(var i=0; i < this.strings;i++ ) {
      let tuningNote = R.NOTES[this.tuning[5-i]];
      if( i === 5 )
        tuningNote = tuningNote.toLowerCase();
      centeredText(c,margin/2,margin+step.height*(5-i),step.width/2,step.width/2-(i/2),tuningNote,'white');
    }
    this.drawFrets(c,fx,fy,fw,fh);
    this.drawStrings(c,fx,fy,fw,fh);
    this.drawDots(c,fx,fy,fw,fh);

    if( this.scale[0] !== null && this.scale[0] !== undefined && this.scale[1] ) {
      for(i=0; i < this.strings;i++ ) {
        const scaleNotes = R.scale2Notes(this.scale[0],this.scale[1]);
        for(j=0; j < this.frets-1;j++ ) {
          let note = (this.tuning[(5-i)]+j+1)%12;
          if( barre !== 0 )
            note = ((note+(barre-1))%12);
          if( scaleNotes.includes(note) ) {
            const cx = step.width*(j+0.5)-step.width*0.05;
            const cy = step.height*(5-i)-step.height*0.05;
            const r = step.width/2*0.75;
            transparentCircle(c,fx+cx,fy+cy,r,SELECTED_SCALE_COLOR,1);
            centeredText(c,fx+cx,fy+cy,r,r,R.NOTES[note],'white',TEXT_SHADOW);
          }
        }
      }
    }

    if( fingers ) {
      for(i=0; i < this.strings;i++ ) {
        const ch = fingers.charAt(i);
        if( ch === R.SYMBOL_CLOSED )
          centeredText(c,margin*1.5,margin+step.height*(5-i),step.width/2,step.width/2,ch,SELECTED_CHORD_COLOR);
        else if( ch === R.SYMBOL_OPEN || ch == 0 )
          centeredText(c,margin*1.5,margin+step.height*(5-i),step.width/2,step.width/2,ch,'gray');
      }

      if( barre !== 0 ) {
        c.strokeStyle = SELECTED_CHORD_COLOR;
        c.lineWidth = step.width/2*0.2;
        c.beginPath();
        c.moveTo(fx+step.width*0.5,fy);
        c.lineTo(fx+step.width*0.5,fy+fh);
        c.stroke();
        c.closePath();
      }

      for(i=0; i < fingers.length;i++ ) {
        const ch = fingers.charAt(i);
        const k = 5- i;
        let calculatedNote;
        let symbol;
        if( ch === R.SYMBOL_CLOSED ){
        } else if( barre !== 0 && ch == 0 ) {
          calculatedNote = (this.tuning[k] + barre)%12;
          symbol = R.NOTES[calculatedNote];
          const cx = fx+step.width*(0.5);
          const cy = fy+(k)*step.height;
          transparentCircle(c,cx,cy,step.width/2,SELECTED_CHORD_COLOR)
          centeredText(c,cx,cy,step.width/2,step.width/2,symbol,'white',TEXT_SHADOW);
        } else if( ch !== '0' ) {
          const fret = parseInt(ch,10);
          calculatedNote = (this.tuning[k] + barre +parseInt(ch,10))%12;
          symbol = R.NOTES[calculatedNote];
          let cx = fx+step.width*(-0.5+fret);
          let cy = fy+step.height*(k);
          if( barre !== 0 )
            cx += step.width;
          transparentCircle(c,cx,cy,step.width/2,SELECTED_CHORD_COLOR)
          centeredText(c,cx,cy,step.width/2,step.width/2,symbol,'white',TEXT_SHADOW);
        }
      }
    }
    if( this.overlays ) {
      this.overlays.forEach(overlay => {
        overlay.draw(c);
      });
    }
  }
}

export class Octave {
  constructor(w, h) {
    this.keyWidth = w;
    this.keyHeight = h;
    this.scale = null;
    this.chord = null;
  }

  draw(c, chordNotes, scaleNotes) {
    const keys = R.WHITES.concat(R.BLACKS);
    for(let i=0; i < 12;i++ ) {
      let f = null;
      let w = this.keyWidth;
      let halfW = w/ 2;
      let h = this.keyHeight;
      let x = i* w;
      const y = 0;
      if( R.PIANO_WHITES[keys[i]] ) {
        f = whiteFill(c,x,y,w,h);
      } else {
        f = blackFill(c,x,y,w,h);
        h = h*0.65;
        x =	(parseInt(keys[i]/2,10)+0.7)*w;
        w = w*0.75;
        halfW = w/2;
      }

      key(c,x,0,w,h,f,'black');
      if( scaleNotes && scaleNotes.includes(keys[i]) ) {
        transparentCircle(c,x+halfW,h-3*halfW,halfW,SELECTED_SCALE_COLOR,1);
        centeredText(c,x+halfW,h-3*halfW,halfW,halfW,R.NOTES[keys[i]],'white',TEXT_SHADOW);
      }
      if( chordNotes && chordNotes.length && chordNotes.includes(keys[i]) ) {
        transparentCircle(c,x+halfW,h-halfW,halfW,SELECTED_CHORD_COLOR,1);
        centeredText(c,x+halfW,h-halfW,halfW,halfW,R.NOTES[keys[i]],'white',TEXT_SHADOW);
      }
    }
  }
}

export class Piano extends Instrument {

  constructor() {
    super();
    this.octaves = [new Octave(),new Octave()];
  }

  draw(c) {
    const w = c.canvas.width;
    const h = c.canvas.height;
    const root = this.chord[0];

    const //chordNotes = R.chord2Notes(this.chord[0],this.chord[1]),
      scaleNotes = (this.scale[0] !== null && this.scale[1] !== null) ? R.scale2Notes(this.scale[0],this.scale[1]) : null;

    let chordNotes = null;
    if( this.chord[0] !== null && this.chord[0] !== undefined && this.chord[1] ) {
      const a = R.CHORD_MAP[this.chord[1]].slice();
      const lastNote = a[a.length-1]+root;
      if( lastNote < 12 )
        this.octaves = [new Octave(),new Octave()];
      else if( lastNote >= 12 && lastNote < 24 )
        this.octaves = [new Octave(),new Octave()];
      else if( lastNote >= 24 && lastNote < 36 )
        this.octaves = [new Octave(),new Octave(),new Octave()];
      a.forEach((arrayValue, arrayIndex, arrayObject) => {
        arrayObject[arrayIndex] = root+arrayValue;
      });
      chordNotes = a;
    }
    const len = this.octaves.length;
    this.octaves.forEach((o, i) => {
      o.keyWidth = w / (len*7);
      o.keyHeight = h;
      c.save();
      c.translate(i*w/len,0);
      o.draw(c,chordNotes,scaleNotes);
      if( chordNotes ) {
        chordNotes.forEach((arrayValue, arrayIndex, arrayObject) => {
          arrayObject[arrayIndex] = arrayValue-12;
        });
      }
      c.restore();
    });
    gloss(c,w,h);
  }
}

export class Overlay {
  constructor(text) {
    this.text = text;
  }

  draw(c) {
    c.globalAlpha = 0.5;
    c.fillStyle = 'black';
    const w = c.canvas.width;
    const h = c.canvas.height;
    const ow = w/4;
    const oh = h/4;
    c.fillRect(w-ow,0, ow, oh);
    c.globalAlpha = 1;
    centeredText(c,w-ow/2,oh/2, ow,oh,this.text,"white");
  }
}