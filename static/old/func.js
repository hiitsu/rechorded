(function () {

	function bindTo(R) {
		R.noteNumber = function(s){
			var octave = s.charAt(s.length-1),
				literal = s.substring(0,s.length-1),
				note = R.NOTES.indexOf(literal.toLocaleUpperCase());
			return octave*12+note;
		};
		R.randomColor = function() {
			return '#'+(Math.random()*0xFFFFFF<<0).toString(16);
		};

		R.permutations = function(v, m){ //v1.0
			for(var p = -1, j, k, f, r, l = v.length, q = 1, i = l + 1; --i; q *= i);
			for(var x = [new Array(l), new Array(l), new Array(l), new Array(l)], j = q, k = l + 1, i = -1;
				++i < l; x[2][i] = i, x[1][i] = x[0][i] = j /= --k);
			for(r = new Array(q); ++p < q;)
				for(r[p] = new Array(l), i = -1; ++i < l; !--x[1][i] && (x[1][i] = x[0][i],
					x[2][i] = (x[2][i] + 1) % l), r[p][i] = m ? x[3][i] : v[x[3][i]])
					for(x[3][i] = x[2][i], f = 0; !f; f = !f)
						for(j = i; j; x[3][--j] == x[2][i] && (x[3][i] = x[2][i] = (x[2][i] + 1) % l, f = 1));
			return r;
		};
		R.encode = function(num,at,value,mask) {
			var inverse = ~(mask << at);
			var encoded = (value << at);
			return ((num & inverse) | encoded);
		};
		R.decode = function(num,at,mask) {
			var n = mask << at;
			return (n & num) >>> at;
		};
		function go(lookup,s,a,k) {
			if (k == a.length) {
				lookup[s] = true;
			} else {
				for(var i=0; i < a[k].length;i++) {
					go(lookup,s + a[k][i], a, k + 1);
				}
			}
		};
		R.possibilities = function(a) {
			var o = {};
			go(o,'',a,0);
			return Object.keys(o);
		};
		R.transposeIgnoreOctave = function(n,semitones){
			var r = n+semitones;
			if( r < 0 ) {
				r = 12 + r;
			}
			return r%12;
		};
		R.difference = function(n,m) {
			var d = Math.abs(m - n);
			if( d > 6 ) d = 6-(d%6);
			return d;
		};
		R.chord2Notes = function(root,type) {
			var a = [],
				steps = R.CHORD_MAP[typeof type === 'string' ? type : R.CHORDS[type]];
			for( var m = 0; m < steps.length; m++ ){
				a.push(R.transposeIgnoreOctave(root,steps[m]));
			}
			return a;
		};
		R.scale2Notes = function(_root,type) {
			var a = [],
				steps = R.SCALE_MAP[typeof type === 'string' ? type : R.SCALES[type]],
				root = typeof _root === 'string' ? R.NOTES.indexOf(_root) : _root;
			for( var m = 0; m < steps.length; m++ ){
				a.push(R.transposeIgnoreOctave(root,steps[m]));
			}
			return a;
		};
	};

	if (typeof module !== 'undefined' && module.exports) {
		module.exports = bindTo;
	} else {
		var R = this.window.R = this.window.R || {};
		bindTo(R);
	}

}());

/*
 R.possibilities = function(arrays) {
 var divisors = [];
 for (var i = arrays.length - 1; i >= 0; i--) {
 divisors[i] = divisors[i + 1] ? divisors[i + 1] * arrays[i + 1].length : 1;
 }

 }



 function getPermutation(n) {
 var result = "", curArray;

 for (var i = 0; i < allArrays.length; i++) {
 curArray = allArrays[i];
 result += curArray[Math.floor(n / divisors[i]) % curArray.length];
 }

 return result;
 }*/