$(function(){
	var o = {
		tuningName:R.TUNINGS[0],
		root:0,
		type:R.CHORDS[0],
		i:0,
		a:[{ root:0, type:'maj' }]
	};
	var selectedRoot = $('.chordRoot:first'),
		selectedType = $('.chordType:first'),
		selectedTuning = $('.tuningName:first');
	$('.tuningName').click(function(e){
		selectedTuning.removeClass('selected');
		selectedTuning = $(this);
		selectedTuning.addClass('selected');
		o.tuningName = selectedTuning.text();
		R.setTuning(o.tuningName);
		if( selectedType )
			selectedType.click();
	});
	$('.chordRoot').click(function(e){
		selectedRoot.removeClass('selected');
		$(this).addClass('selected');
		selectedRoot = $(this);
		o.root = R.NOTES.indexOf($(this).text());
		queryAndUpdateUI();
		e.preventDefault();
		return false;
	});
	var guitarCanvas = $('canvas[data-drawable="Guitar"]');
	guitarCanvas.click(function(){
		o.i++;
		if( o.a.length <= o.i )
			o.i = 0;
		updateUI();
	});
	$('.chordType').click(function(e){
		selectedType.removeClass('selected');
		$(this).addClass('selected');
		selectedType = $(this);
		o.type = $(this).text();
		queryAndUpdateUI();
		e.preventDefault();
		return false;
	});
	function queryAndUpdateUI() {
		$.get('/data?tuningName='+o.tuningName+'&chordRoot='+ o.root+'&chordType='+ o.type,function(rows){
			o.a = rows;
			o.i = 0;
			updateUI();
		});
	}
	function updateUI() {
		var d = R.drawableFor(guitarCanvas.get(0).getContext('2d'));
		if(o.a && o.a.length) {
			var c = o.a[o.i];
			d.setOverlay((o.i +1)+"/"+o.a.length);
			R.setChord(c.root,c.type, c.barre, c.fingers);
		} else {
			d.setOverlay("none");
			R.setChord(o.root,o.type);
		}
	}

	selectedRoot.addClass('selected');
	selectedType.addClass('selected');
	selectedTuning.addClass('selected');
	queryAndUpdateUI();
});