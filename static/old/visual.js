var SELECTED_CHORD_COLOR = "rgb(203, 11, 33)",
	SELECTED_SCALE_COLOR = "rgb(11, 222, 33)",
	FONT_FAMILY = "Sansus Webissimo",
	TEXT_SHADOW = {
		color:"rgb(0, 0, 0)",
		ox:2.0,
		oy:2.0,
		blur:1
	};

function font(c,h) {
	c.font = h+'px '+FONT_FAMILY;
}

function fillRect(c,x,y,w,h,radius){
	fillRectR(c,x-w/2,y-h/2,w,h,radius)
}

function fillRectR(c,x,y,w,h,radius){
	var	r = radius || 5;
	c.beginPath();
	c.moveTo(x + r, y);
	c.lineTo(x + w - r, y);
	c.quadraticCurveTo(x + w, y, x + w, y + r);
	c.lineTo(x + w, y + h - r);
	c.quadraticCurveTo(x + w, y + h, x + w - r, y + h);
	c.lineTo(x + r, y + h);
	c.quadraticCurveTo(x, y + h, x, y + h - r);
	c.lineTo(x, y + r);
	c.quadraticCurveTo(x, y, x + r, y);
	c.closePath();
	c.fill();
};

function background(c,w,h){
	c.save();
	c.beginPath();
	c.moveTo(w, h);
	c.lineTo(0, h);
	c.lineTo(0, 0);
	c.lineTo(w, 0);
	c.lineTo(w, h);
	c.closePath();
	var gradient = c.createRadialGradient(w/2.0, h/2, 0.0, w/2,h/2, w/2-w/8);
	gradient.addColorStop(0.00, "rgb(0, 0, 0)");
	gradient.addColorStop(1.00, "rgb(36,36,36)");
	c.fillStyle = gradient;
	c.fill();
	c.restore();
}
function transparentCircle(c,x,y,r,fillStyle,a) {
	c.save();
	c.translate(x,y);
	c.globalAlpha = (a ? a : 1);
	c.fillStyle = fillStyle || 'blue';
	c.beginPath();
	c.arc(0,0,r, 0, Math.PI * 2, true );
	c.closePath();
	c.fill();
	c.restore();
}
function guitarDarkBrown(c,x,y,w,h) {
	c.save();
	//c.globalAlpha = 1.0;
	c.translate(x, y-5);
	/*c.shadowColor = "rgba(0, 0, 0, 0.75)";
	c.shadowOffsetX = 0.0;
	c.shadowOffsetY = 2.0;
	c.shadowBlur = 4.0;
	c.fillStyle = "rgb(47, 23, 17)";
	c.fillRect(0,0,w,h);*/
	c.globalAlpha = 1;
	var g = c.createLinearGradient(w,0,w,h);
	g.addColorStop(0.00, "rgb(148, 148, 148)");
	//g.addColorStop(0.28, "rgb(155, 77, 33)");
	//g.addColorStop(0.74, "rgb(155, 77, 33)");
	g.addColorStop(1.00, "rgb(34, 34, 34)");
	c.fillStyle = g;
	c.fillRect(0,0,w,h+10);
	//c.fill();
	c.globalAlpha = 1.0;
	c.restore();
}
function line(c,x,y,len,angle,fatness,fillStyle,strokeStyle,shadow){
	c.save();
	c.translate(x,y);
	c.rotate(angle);
	if( shadow ) {
		c.shadowColor = shadow.color;
		c.shadowOffsetX = shadow.ox;
		c.shadowOffsetY = shadow.oy;
		c.shadowBlur = shadow.blur;
	}
	c.beginPath();
	var a = fatness/2;
	c.moveTo(-a,0);
	c.lineTo(a,0);
	c.lineTo(a,len);
	c.lineTo(-a,len);
	c.lineTo(-a,0);
	c.closePath();
	if( fillStyle ) {
		c.fillStyle = fillStyle;
		c.fill();
	}
	if( strokeStyle ) {
		c.strokeStyle = strokeStyle;
		c.stroke();
	}
	c.restore();
}
function guitarFret(c,x,y,len,angle,fatness) {
	var g = c.createLinearGradient(-fatness/2,len,fatness/2,len);
	g.addColorStop(0.00, "#533");
	g.addColorStop(0.5, "#fee");
	g.addColorStop(1.00, "#a88");
	line(c,x,y,len,angle,fatness,g,null,{
		color:"rgba(0, 0, 0, 0.5)",
		ox:2.0,
		oy:0.0,
		blur:2.0
	});

}
function guitarString(c,x,y,len,angle,fatness,shadowSize) {
	var g = c.createLinearGradient(-fatness/2,len,fatness/2,len);
	g.addColorStop(0.00, "rgb(149,149,149)");
	g.addColorStop(0.5, "#fff");
	g.addColorStop(1.00, "rgb(149,149,149)");
	line(c,x,y,len,angle,fatness,"white",null,{
		color:"rgba(0, 0, 0, 0.5)",
		ox:0.0,
		oy:4,
		blur:2.0
	});
	/*line(c,x,y,len,angle,1+fatness/2,g,null,{
		color:"rgba(0, 0, 0, 0.5)",
		ox:0.0,
		oy:4,
		blur:2.0
	});*/
}
function ellipse(c, x, y, w, h,fillStyle,strokeStyle) {
	c.save();
	var kappa = .5522848;
	ox = (w / 2) * kappa, // control point offset horizontal
		oy = (h / 2) * kappa, // control point offset vertical
		xe = x + w,           // x-end
		ye = y + h,           // y-end
		xm = x + w / 2,       // x-middle
		ym = y + h / 2;       // y-middle

	c.beginPath();
	c.moveTo(x, ym);
	c.bezierCurveTo(x, ym - oy, xm - ox, y, xm, y);
	c.bezierCurveTo(xm + ox, y, xe, ym - oy, xe, ym);
	c.bezierCurveTo(xe, ym + oy, xm + ox, ye, xm, ye);
	c.bezierCurveTo(xm - ox, ye, x, ym + oy, x, ym);
	c.closePath();
	if( fillStyle ) {
		c.fillStyle = fillStyle;
		c.fill();
	}
	if( strokeStyle ) {
		c.strokeStyle = strokeStyle;
		c.stroke();
	}
	c.restore();
}
function metalCircle(c,x,y,r) {
	c.save();
	var g = c.createRadialGradient(x,y,0,x,y,r);
	g.addColorStop(0.00, "rgb(203, 203, 203)");
	g.addColorStop(0.64, "rgb(179, 179, 179)");
	g.addColorStop(1.00, "rgb(152, 152, 152)");
	c.fillStyle = g;
	c.beginPath();
	c.arc(x,y,r, 0, Math.PI * 2, true );
	c.closePath();
	c.fill();
	c.restore();
}
function key(c,x,y,w,h,fill,stroke) {
	c.save();
	c.translate(x,y);
	//c.shadowColor = "rgba(0, 0, 0, 0.8)";
	//c.shadowOffsetX = 2.0;
	//c.shadowOffsetY = 2.0;
	//c.shadowBlur = 4.0;
	c.beginPath();
	c.moveTo(0,0);
	c.lineTo(0,h);
	c.lineTo(w, h);
	c.lineTo(w,0);
	c.lineTo(0,0);
	c.closePath();

	if( fill ) {
		c.fillStyle = fill || 'orange';
		c.fill();
	}
	if( stroke ) {
		c.strokeStyle = stroke || 'blue';
		c.stroke();
	}
	c.restore();
}
function blackFill(c,x,y,w,h) {
	var g = c.createLinearGradient(w/2, 0, w/2, h);
	g.addColorStop(0.33, "rgb(76, 76, 76)");
	g.addColorStop(1.00, "rgb(0, 0, 0)");
	return g;
}
function whiteFill(c,x,y,w,h) {
	var g = c.createLinearGradient(w/2, h, w/2, 0);
	g.addColorStop(0.77, "rgb(255, 255, 255)");
	g.addColorStop(1.00, "rgb(178, 178, 178)");
	return g;
}

function letterBall(c,x,y,r,ch,bgFill,textFill,shadow) {
	c.save();
	c.translate(x,y);
	c.beginPath();
	c.arc(0,0,r, 0, Math.PI * 2, true );
	c.closePath();
	c.fillStyle = bgFill || 'black';
	c.fill();
	c.restore();
	centeredText(c,x,y,r*2,r*2,ch,textFill || 'white',shadow);
}
function centeredText(c,x,y,w,h,t,fillStyle,shadow) {
	c.save();
	c.translate(x,y);
	if( shadow ) {
		c.shadowColor = shadow.color;
		c.shadowOffsetX = shadow.ox;
		c.shadowOffsetY = shadow.oy;
		c.shadowBlur = shadow.blur;
	}
	if( !fillStyle ) {
		var g = c.createLinearGradient(w,0,w,h);
		g.addColorStop(0.00, "rgb(127, 127, 127)");
		g.addColorStop(1.00, "rgb(255, 255, 255)");
		c.fillStyle = g;
	} else c.fillStyle = fillStyle;
	c.textAlign = 'center';
	var fontHeight = parseInt(h,10);
	font(c,fontHeight);
	c.fillText(t,0,(h/2)*0.7,w);
	c.restore();
}
function letterBox(c,x,y,w,h,ch,shadow) {
	c.save();
	c.translate(x,y);
	c.fillStyle = 'black';
	c.fillRect(-w/2,-h/2,w,h);
	c.restore();
	centeredText(c,x,y,w,h,ch,'white',shadow);
}

function gloss(c,w,h){
	c.save();
	c.globalAlpha = 0.10;
	c.beginPath();
	c.moveTo(0, 0);
	c.lineTo(0, h/4);
	c.lineTo(w, h/3);
	c.lineTo(w, 0);
	c.lineTo(0, 0);
	c.closePath();
	c.fillStyle = "rgb(255, 255, 255)";
	c.fill();
	c.globalAlpha = 1.0;
	c.restore();
}
function label(c,x,y,w,h,text,bgFill,textfill){
	fillRect(c,x,y,w,h,20,'black');
	centeredText(c,x,y,w,h,text,textfill);
}

function Drawable(w,h,resizeToFit) {
	this.w = w || 0.5;
	this.h = h || 0.5;
	this.resizeToFit = resizeToFit;
	this.overlays = [];
}
/**
 * Do dynamic size adjustments for the canvas accordingly.
 * @param 	c			Canvas context
 * @return 	{Object}	Return w and h used for drawing
 */
Drawable.prototype.adjust = function(c) {

	//var windowWidth = c.canvas.parentNode.clientWidth,
	//	windowHeight = c.canvas.parentNode.clientHeight,
	if( this.resizeToFit ) {
		var w = c.canvas.parentNode.clientWidth,
			h = c.canvas.parentNode.clientHeight;
		if( c.canvas.width !== w || c.canvas.height !== h) {
			c.canvas.width = w;
			c.canvas.height = h;
		}
		return { w:w, h:h };
	}
	return { w:c.canvas.width, h:c.canvas.height };
};
Drawable.prototype.setOverlay = function(text) {
	this.overlays = [new Overlay(text)];
};
function Instrument(w,h,resizeToFit) {
	Drawable.call(this,w,h,resizeToFit);
	this.chord = [0,'maj'];
	this.scale = [null,null];
}
Instrument.prototype = new Drawable();
Instrument.prototype.setChord = function(root,type){
	this.chord[0] = typeof root === 'string' ? R.NOTES.indexOf(root) : root;
	this.chord[1] = typeof type === 'string' ? type : R.CHORDS[type];
};
Instrument.prototype.setScale = function(root,type){
	this.scale[0] = root;
	this.scale[1] = type;
};
Instrument.prototype.chordText = function(){
	var root = typeof this.chord[0] !== 'string' ? R.NOTES[this.chord[0]] : this.chord[0],
		type = typeof this.chord[1] !== 'string' ? R.CHORDS[this.chord[1]] : this.chord[1];
	return root + type;
};
Instrument.prototype.scaleText = function(){
	var root = typeof this.scale[0] !== 'string' ? R.NOTES[this.scale[0]] : this.scale[0],
		type = typeof this.scale[1] !== 'string' ? R.SCALES[this.scale[1]] : this.scale[1];
	return root +"-"+ type;
};
function Guitar(w,h,tuningName,resizeToFit){
	Instrument.call(this,w,h,resizeToFit);
	this.setTuning(tuningName);
	this.dots = [2,5,9];
	this.doubledots = [7];
	this.frets = 7;
	this.strings = 6;
	this.s = null;
	this.barre = 0;
}
Guitar.prototype = new Instrument();
Guitar.prototype.setTuning = function(tuningName){
	this.tuning = tuningName ? R.TUNING_MAP[tuningName].slice(0).reverse() : R.TUNING_MAP[R.DEFAULT_TUNING].slice(0).reverse();
};
Guitar.prototype.setChord = function(root,type,barre,s){
	this.chord[0] = this.chord[0];
	this.chord[1] = this.chord[1];
	this.barre = barre || 0;
	this.s = s;
};
Guitar.prototype.step = function(x,y,w,h){
	var xx = w/(this.frets-1),
		yy = h/(this.strings-1),
		sx = xx > yy ? yy : xx,
		margined = sx*0.8,
		half = sx/2*0.8;
	return { x:xx,y:yy,sx:sx,margined:margined,half:half };
};
Guitar.prototype.drawFrets = function(c,x,y,w,h){
	var len = this.frets, step = this.step(x,y,w,h);
	for(var j=0; j < len;j++ ) {
		var xpos = x+step.x*j;
		guitarFret(c,xpos,y-4,h+8,0,step.x*0.08);
	}
};
Guitar.prototype.drawStrings = function(c,x,y,w,h){
	var len = this.strings, step = this.step(x,y,w,h), fatnessBase = 1;
	for(var j=0; j < len;j++ ) {
		var ypos = y+step.y* j, fatness = j/2+fatnessBase;
		guitarString(c,x,ypos,w,-Math.PI/2,fatness,2);
	}
};
Guitar.prototype.drawDots = function(c,x,y,w,h){
	var len = this.dots, step = this.step(x,y,w,h);
	for(var j=0; j < len;j++ ) {
		var pos = this.dots[j],
			xpos = x+step.x*pos;
		if( pos < this.barre || pos >= this.barre+this.frets )
			break;
		metalCircle(c,xpos,3.5*step.y,step.half);
	}
	for(j=0; j < this.doubledots.length;j++ ) {
		var pos = this.doubledots[j],
			xpos = x+step.x*pos;
		if( pos < this.barre || pos >= this.barre+this.frets )
			break;
		metalCircle(c,xpos,2.5*step.y, step.half );
		metalCircle(c,xpos,4.5*step.y, step.half );
	}
};
Guitar.prototype.draw = function(c){
	var o = this.adjust(c),
		h = o.h,
		w = o.w;
	background(c,w,h);

	var smaller = w > h ? h : w,
		margin = smaller/20,
		fx = margin*2,
		fy = margin,
		fw = w-margin*3,
		fh = h-margin*2,
		fingers = this.s,
		barre = this.barre,
		step = this.step(fx,fy,fw,fh);
	guitarDarkBrown(c,fx,fy,fw,fh);
	for(var j=0; j < this.frets-1;j++ ) {
		var num = barre === 0 ? (j+1) : barre+j;
		letterBall(c,fx+(j+0.5)*step.x,margin/2,margin/2,''+num,"rgb(36,36,36)","white");
	}
	for(var i=0; i < this.strings;i++ ) {
		var tuningNote = R.NOTES[this.tuning[5-i]];
		if( i === 5 )
			tuningNote = tuningNote.toLowerCase();
		centeredText(c,margin/2,margin+step.y*(5-i),step.half,step.half-(i/2),tuningNote,'white');
	}
	this.drawFrets(c,fx,fy,fw,fh);
	this.drawStrings(c,fx,fy,fw,fh);
	this.drawDots(c,fx,fy,fw,fh);

	if( this.scale[0] !== null && this.scale[0] !== undefined && this.scale[1] ) {
		for(i=0; i < this.strings;i++ ) {
			var scaleNotes = R.scale2Notes(this.scale[0],this.scale[1]);
			for(j=0; j < this.frets-1;j++ ) {
				var note = (this.tuning[(5-i)]+j+1)%12;
				if( barre !== 0 )
					note = ((note+(barre-1))%12);
				if( scaleNotes.indexOf(note) !== -1 ) {
					var cx = step.x*(j+0.5)-step.x*0.05,
						cy = step.y*(5-i)-step.y*0.05,
						r = step.half*0.75;
					transparentCircle(c,fx+cx,fy+cy,r,SELECTED_SCALE_COLOR,1);
					centeredText(c,fx+cx,fy+cy,r,r,R.NOTES[note],'white',TEXT_SHADOW);
				}
			}
		}
	}

	if( fingers ) {
		for(i=0; i < this.strings;i++ ) {
			var ch = fingers.charAt(i);
			if( ch === R.SYMBOL_CLOSED )
				centeredText(c,margin*1.5,margin+step.y*(5-i),step.half,step.half,ch,SELECTED_CHORD_COLOR);
			else if(  ch === R.SYMBOL_OPEN || ch == 0 )
				centeredText(c,margin*1.5,margin+step.y*(5-i),step.half,step.half,ch,'gray');
		}

		if( barre !== 0 ) {
			c.strokeStyle = SELECTED_CHORD_COLOR;
			c.lineWidth = step.half*0.2;
			c.beginPath();
			c.moveTo(fx+step.x*0.5,fy);
			c.lineTo(fx+step.x*0.5,fy+fh);
			c.stroke();
			c.closePath();
		}

		for(i=0; i < fingers.length;i++ ) {
			var ch = fingers.charAt(i),
				k = 5- i,
				calculatedNote, symbol;
			if( ch === R.SYMBOL_CLOSED ){
			} else if( barre !== 0 && ch == 0 ) {
				calculatedNote = (this.tuning[k] + barre)%12;
				symbol = R.NOTES[calculatedNote];
				var cx = fx+step.x*(0.5),
					cy = fy+(k)*step.y;
				transparentCircle(c,cx,cy,step.half,SELECTED_CHORD_COLOR)
				centeredText(c,cx,cy,step.half,step.half,symbol,'white',TEXT_SHADOW);
			} else if( ch !== '0' ) {
				var fret = parseInt(ch,10);
					calculatedNote = (this.tuning[k] + barre +parseInt(ch,10))%12;
					symbol = R.NOTES[calculatedNote];
				var cx = fx+step.x*(-0.5+fret),
					cy = fy+step.y*(k);
				if( barre !== 0 )
					cx += step.x;
				transparentCircle(c,cx,cy,step.half,SELECTED_CHORD_COLOR)
				centeredText(c,cx,cy,step.half,step.half,symbol,'white',TEXT_SHADOW);
			}
		}
	}
	if( this.overlays ) {
		this.overlays.forEach(function(overlay){
			overlay.draw(c);
		});
	}
};
function Octave(w,h){
	this.keyWidth = w;
	this.keyHeight = h;
	this.scale = null;
	this.chord = null;
}
Octave.prototype.draw = function(c,chordNotes,scaleNotes) {
	var keys = R.WHITES.concat(R.BLACKS);
	for(var i=0; i < 12;i++ ) {
		var f = null,
			w = this.keyWidth,
			halfW = w/ 2,
			h = this.keyHeight,
			x = i* w, y = 0;
		if( R.PIANO_WHITES[keys[i]] ) {
			f = whiteFill(c,x,y,w,h);
		} else {
			f = blackFill(c,x,y,w,h);
			h = h*0.65;
			x =	(parseInt(keys[i]/2,10)+0.7)*w;
			w = w*0.75;
			halfW = w/2;
		}

		key(c,x,0,w,h,f,'black');
		if( scaleNotes && scaleNotes.indexOf(keys[i]) !== -1 ) {
			transparentCircle(c,x+halfW,h-3*halfW,halfW,SELECTED_SCALE_COLOR,1);
			centeredText(c,x+halfW,h-3*halfW,halfW,halfW,R.NOTES[keys[i]],'white',TEXT_SHADOW);
		}
		if( chordNotes && chordNotes.length && chordNotes.indexOf(keys[i]) !== -1 ) {
			transparentCircle(c,x+halfW,h-halfW,halfW,SELECTED_CHORD_COLOR,1);
			centeredText(c,x+halfW,h-halfW,halfW,halfW,R.NOTES[keys[i]],'white',TEXT_SHADOW);
		}


	}
};

function Piano(w,h,resizeToFit){
	Instrument.call(this,w,h,resizeToFit);
	this.octaves = [new Octave(),new Octave()];
}
Piano.prototype = new Instrument();

Piano.prototype.draw = function(c) {
	var o = this.adjust(c),
		h = o.h,
		w = o.w,
		root = this.chord[0],
		//chordNotes = R.chord2Notes(this.chord[0],this.chord[1]),
		scaleNotes = (this.scale[0] !== null && this.scale[1] !== null) ? R.scale2Notes(this.scale[0],this.scale[1]) : null,
		chordNotes = null;
	if( this.chord[0] !== null && this.chord[0] !== undefined && this.chord[1] ) {
		var a = R.CHORD_MAP[this.chord[1]].slice(),
			lastNote = a[a.length-1]+root;
		if( lastNote < 12 )
			this.octaves = [new Octave(),new Octave()];
		else if( lastNote >= 12 && lastNote < 24 )
			this.octaves = [new Octave(),new Octave()];
		else if( lastNote >= 24 && lastNote < 36 )
			this.octaves = [new Octave(),new Octave(),new Octave()];
		a.forEach(function(arrayValue,arrayIndex,arrayObject){
			arrayObject[arrayIndex] = root+arrayValue;
		});
		chordNotes = a;
	}
	var len = this.octaves.length;
	this.octaves.forEach(function(o,i){
		o.keyWidth = w / (len*7);
		o.keyHeight = h;
		c.save();
		c.translate(i*w/len,0);
		o.draw(c,chordNotes,scaleNotes);
		if( chordNotes ) {
			chordNotes.forEach(function(arrayValue,arrayIndex,arrayObject){
				arrayObject[arrayIndex] = arrayValue-12;
			});
		}
		c.restore();
	});
	gloss(c,w,h);
};

function Overlay(text) {
	this.text = text;
}
Overlay.prototype.draw = function(c) {
	c.globalAlpha = 0.5;
	c.fillStyle = 'black';
	var w = c.canvas.width,
		h = c.canvas.height,
		ow = w/4,
		oh = h/4;
	c.fillRect(w-ow,0, ow, oh);
	c.globalAlpha = 1;
	centeredText(c,w-ow/2,oh/2, ow,oh,this.text,"white");
}