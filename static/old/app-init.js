$(function(){
	$('canvas.drawable').each(function(index, canvas){
		var self = $(canvas),
			ctor = self.attr('data-drawable');
		var drawable = new window[ctor](100,100,false);
		R.registerDrawable(drawable,canvas.getContext('2d'));
		R.setChord(null,null);
	});
	$(window).resize(function(){
		R.draw();
	});
});
