var fs = require('fs');

var R = {};
require('./constants.js')(R);
require('./func.js')(R);
require('./rechorded.js')(R);

var store = new R.Store();
store.findAll();

fs.writeFileSync("chordbank.json", JSON.stringify(store.filter(), null, 2));
fs.writeFileSync("scalemap.json", JSON.stringify(R.SCALE_MAP, null, 2));
fs.writeFileSync("chordmap.json", JSON.stringify(R.CHORD_MAP, null, 2));
fs.writeFileSync("genremap.json", JSON.stringify(R.GENRE_MAP, null, 2));
fs.writeFileSync("scales.json", JSON.stringify(R.SCALES, null, 2));
fs.writeFileSync("chordtypes.json", JSON.stringify(R.CHORDS, null, 2));
fs.writeFileSync("genres.json", JSON.stringify(R.GENRES, null, 2));