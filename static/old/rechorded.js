(function () {

	function bindTo(R) {

		function Chord(root,type){
			if( typeof root === 'string' )
				this.root = R.NOTES.indexOf(root);
			else
				this.root = root || 0;

			if( typeof type === 'string' )
				this.type = R.CHORDS.indexOf(type);
			else
				this.type = type || 0;
		}
		Chord.prototype.hash = function() {
			return this.root*1000 + this.type;
		};
		Chord.prototype.transposeIgnoreOctave = function(d) {
			this.root = R.transposeIgnoreOctave(this.root,d);
		};
		Chord.prototype.noteMatch = function(a){
			var steps = R.CHORD_MAP[R.CHORDS[this.type]], found = {};
			for(var j=0; j < a.length;j++) {
				var flag = false;
				for(var i=0; i < steps.length;i++) {
					if( (this.root + steps[i])%12 === a[j] ) {
						found[a[j]] = true;
						flag = true;
					}
				}
				if( !flag )
					return false;
			}
			return Object.keys(found).length === steps.length;
		};
		Chord.prototype.noteCount = function(){
			return R.CHORD_MAP[R.CHORDS[this.type]].length;
		};
		Chord.prototype.hasNote = function(n){
			var steps = R.CHORD_MAP[R.CHORDS[this.type]],m;
			for(m = 0; m < steps.length; m++ ){
				if( (this.root + steps[m]) %12 === n%12 )
					return true;
			}
			return false;
		};
		Chord.prototype.notesIgnoreOctave = function() {
			return R.chord2Notes(this.root,this.type);
		};
		Chord.prototype.scales = function() {
			var a = [], self = this;
			R.NOTES.forEach(function(root,i){
				R.SCALES.forEach(function(type,j){
					var scaleNotes = R.scale2Notes(root,type),
						chordNotes = self.notesIgnoreOctave(),
						flag = true;
					chordNotes.forEach(function(n){
						if( scaleNotes.indexOf(n) === -1 )
							flag = false;
					});
					if( flag )
						a.push([i,j]);
				});
			});
			return a;
		};
		Chord.prototype.genres = function() {
			var a = [], self = this;
			R.GENRES.forEach(function(genre){
				var types = R.GENRE_MAP[genre];
				if( types.indexOf(R.CHORDS[self.type]) !== -1 )
					a.push(genre);
			});
			return a;
		};
		function GuitarChord(root,type,barre,s,tuningName) {
			Chord.call(this,root,type);
			this.s = s || 'XXXXXX';
			this.barre = parseInt(barre,10) || 0;
			this.tuningName = tuningName || R.DEFAULT_TUNING;
			return this;
		}
		GuitarChord.prototype = new Chord();
		GuitarChord.prototype.difficulty = function() {
			var a = this.barre == 0 ? 0 : 3,
				b = this.fingersUsed(),
				c = this.playability(),
				d = this.fingerPlacementDifficulty();
			return parseInt(a+b+c+d,10);
		};
		GuitarChord.prototype.fingerPlacementDifficulty = function() {
			var sum = 0, mul = 1;
			if( this.barre )
				mul = 1.5;
			var a = this.s.match(/3/g),
				b = this.s.match(/2/g),
				c = this.s.match(/1/g);
			if( a )
				sum = sum + a.length*0.75;
			if( b )
				sum = sum + b.length*0.3;
			if( c )
				sum = sum + c.length*0.2;
			return sum*mul;
		};
		GuitarChord.prototype.playability = function() {
			if( /^[^X]{6}$/g.test(this.s) )
				return 0;
			if( /^X[^X]{5}$/g.test(this.s) || /^XX[^X]{4}$/g.test(this.s) )
				return 1;
			if( /^X[^X]{4}X$/g.test(this.s) || /^[^X]{1}X[^X]{3}X$/g.test(this.s) )
				return 2.5;
			if( /^XXX[^X]{3}$/g.test(this.s) )
				return 1.5;
			if( /^[^X]{5}X$/g.test(this.s) )
				return 1.5;
			if( /^[^X]{1}X[^X]{4}$/g.test(this.s) || /^XX[^X]{3}X$/g.test(this.s) )
				return 2;
			return 0;
		};
		GuitarChord.prototype.lowestStrumNoteIndex = function() {
			var i = this.s.indexOf(R.SYMBOL_CLOSED),
				k = i+1;
			if( i === 5 || i === -1 )
				k = 0;
			return k;
		};
		GuitarChord.prototype.lowestStrumNote = function() {
			var k = this.lowestStrumNoteIndex(),
				tuning = R.TUNING_MAP[this.tuningName];
			return (tuning[k]+this.barre+parseInt(c,10))%12;
		};
		GuitarChord.prototype.lowestOpenNote = function() {
			var tuning = R.TUNING_MAP[this.tuningName];
			for(var i=0; i < 6; i++) {
				var c = this.s.charAt(i);
				if( c !== R.SYMBOL_CLOSED ) {
					return (tuning[i]+this.barre+parseInt(c,10))%12;
				}
			}
			return null;
		};
		GuitarChord.prototype.fingersUsed = function() {
			var count = 0;
			for(var i=0; i < 6; i++) {
				var c = this.s.charAt(i);
				if( c !== R.SYMBOL_CLOSED && c !== R.SYMBOL_OPEN && c != 0 )
					count++;
			}
			if( this.barre !== 0 )
				count++;
			return count;
		};
		GuitarChord.prototype.fretSpanAverage = function() {
			var pattern =/^[1234]$/g,
				sum = 0;
			for(var i=0; i <= 5; i++) {
				if( pattern.test(this.s.charAt(i)) )
					sum += parseInt(this.s.charAt(i),10);
			}
			return sum/4;
		};
		GuitarChord.prototype.isStrummable = function() {
			for(var i=2; i <= 5; i++) {
				if( this.s.charAt(i) === R.SYMBOL_CLOSED )
					return false;
			}
			if( this.lowestOpenNote())
			return true;
		};
		GuitarChord.prototype.closedCount = function() {
			var m = this.s.match(/X/g);
			return m ? m.length : 0;
		};
		GuitarChord.prototype.isValid = function() {
			var tuning = R.TUNING_MAP[this.tuningName];
			if( this.lowestOpenNote() !== this.root )
				return false;
			if( this.fingersUsed() > 4 )
				return false;
			var count3 = this.s.match(/3/g),
				count2 = this.s.match(/2/g),
				count1 = this.s.match(/1/g);
			if( this.barre ) {
				if( count3 && count3.length >= 3 )
					return false;
			}
			if( !this.barre && count1 && count1.length == 2 && count3 && count3.length == 2 )
				return false;

			var	found = [];
			for(var i=0; i < 6; i++) {
				var p = this.s.charAt(i);
				if( p === R.SYMBOL_CLOSED )
					continue;
				else if( p === R.SYMBOL_OPEN ) {
					if( !this.hasNote(tuning[i]) )
						return false;
					found.push(tuning[i]);
				}
				var n = R.transposeIgnoreOctave(tuning[i],this.barre+parseInt(p,10));
				if( !this.hasNote(n) )
					return false;
				found.push(n);
			}
			var result = this.noteMatch(found);
			return result;
		};

		function Fretboard(_tuning) {
			this.tuning = _tuning;
			return this;
		}
		Fretboard.prototype.positions = function(notes,barreFret) {
			var c = [];
			for(var k=0; k < this.tuning.length; k++) {
				var b = [];
				if( k <= 1 || k >= 5 )
					 b.push(R.SYMBOL_CLOSED);
				if( notes.indexOf(this.tuning[k]+barreFret) !== -1 ) {
					if( barreFret === 0 )
						b.push(R.SYMBOL_OPEN);
					else
						b.push(0);
				}
				for(var j=0; j < R.GUITAR_MAX_REACH; j++) {
					if( notes.indexOf((this.tuning[k]+barreFret+j)%12) !== -1 ) {
						b.push(j);
					}
				}
				c.push(b);
			}
			return c;
		};
		Fretboard.prototype.find = function(notes) {
			var o = {};
			for(var i=0; i < R.GUITAR_MAX_FRETS - R.GUITAR_MAX_REACH; i++) {
				o[i] = R.possibilities(this.positions(notes,i));
			}
			return o;
		};

		function Store(){
			this.all = [];
		}

		Store.prototype.findAll = function(){
			var self = this;
			R.TUNINGS.forEach(function(tuningName) {
				R.NOTES.forEach(function(root){
					R.CHORDS.forEach(function(type){
						var chord = new Chord(root,type);
						if( chord.noteCount() <= 6 ) {
							var fretboard = new Fretboard(R.TUNING_MAP[tuningName]);
							var o = fretboard.find(chord.notesIgnoreOctave());
							Object.keys(o).forEach(function(barre){
								o[barre].forEach(function(s){
									var guitarChord = new GuitarChord(root,type,barre,s,tuningName);
									if( guitarChord.isValid() ) {
										self.all.push(guitarChord);
									}
								});
							});
						}
					});
				});
			});
		};

		// exports
		R.GuitarChord = GuitarChord;
		R.Chord = Chord;
		R.Fretboard = Fretboard;
		R.Store = Store;
	}

	if (typeof module !== 'undefined' && module.exports) {
		module.exports = bindTo;
	} else {
		var R = this.R = this.R || {};
		bindTo(R);
	}

}());

