(function () {

	function bindTo(R) {
		R.SYMBOL_CLOSED			= 'X';
		R.SYMBOL_OPEN			= 'F';
		R.GUITAR_MAX_REACH		= 4;
		R.GUITAR_MAX_FRETS		= 22;
		R.TUNING_MAP = {
			'standard'		:	[4,9,2,7,11,4],
			'dropd'			:	[2,9,2,7,11,4],
			'openg'			:	[2,7,2,7,11,2],
			'dropc'			:	[0,7,0,5,9,2]
		};
		R.TUNINGS = Object.keys(R.TUNING_MAP);
		R.DEFAULT_TUNING		= R.TUNINGS[0];
		R.PIANO_WHITES			= [1,0,1,0,1,1,0,1,0,1,0,1];
		R.WHITES				= [0,2,4,5,7,9,11];
		R.BLACKS				= [1,3,6,8,10];
		R.NOTES = ['C','C#','D','D#','E','F','F#','G','G#','A','A#','B'];
		R.SCALE_MAP = {
			'dorian'		:[0, 2, 3, 5, 7, 9, 10],
			'lydian'		:[0, 2, 4, 6, 7, 9, 11],
			'mixolydian'	:[0, 2, 4, 5, 7, 9, 10],
			'ionian'		:[0, 2, 4, 5, 7, 9, 11],
			'aeolian'		:[0, 2, 3, 5, 7, 8, 10],
			'phrygian'		:[0, 1, 3, 5, 7, 8, 10],
			'locrian'		:[0, 1, 3, 5, 6, 8, 10]
			//,'harmonic minor':[0, 2, 3, 5, 7, 8, 11]
		};
		R.SCALES = Object.keys(R.SCALE_MAP);

		R.CHORD_MAP = {
			'2'		: [0,2,4,7],
			'5'		: [0,7],
			'-5'	: [0,4,6],
			'6'		: [0,4,7,9],
			'6/9'	: [0,4,9,13],
			'7'		: [0,4,7,10],
			'9'		: [0,4,7,10,14],
			'11'	: [0,4,7,10,14,17],
	//		'13'	: [0,4,7,10,14,17,21],
			'maj'	: [0,4,7],
			'maj7'	: [0,4,7,11],
			'maj9'	: [0,4,7,11,14],
			'maj11'	: [0,4,7,11,14,17],
	//		'maj13'	: [0,4,7,11,14,17,21],
			'm'		: [0,3,7],
			'm6'	: [0,3,7,9],
			'm7'	: [0,3,7,10],
			'm7-5'	: [0,3,6,10],
			'm9'	: [0,3,7,10,14],
			'm11'	: [0,3,7,10,14,17],
	//		'm13'	: [0,3,7,10,14,17,21],
			'add9'	: [0,4,7,14],
			'madd9'	: [0,3,7,14],
			'6add9'	: [0,4,7,9,14],
			'm6add9': [0,3,7,9,14],
			'7b5'	: [0,4,6,10],
			'7#5'	: [0,4,8,10],
			'7b9'	: [0,4,7,10,13],
			'7#9'	: [0,4,7,10,15],
			'sus4'	: [0,5,7],
			'sus2'	: [0,2,7],
			'7sus4'	: [0,5,7,10],
			'dim'	: [0,3,6],
			'dim7'	: [0,3,6,9]
		};
		R.CHORDS = Object.keys(R.CHORD_MAP);
	/*
		major				maj			0, 4, 7
		minor				min			0, 3, 7
		augmented			aug			0, 4, 8
		diminished			dim			0, 3, 6
		7th (dominant) 		dom7		0, 4, 7, 10
		major 7th			maj7		0, 4, 7, 11
		minor 7th			min7		0, 3, 7, 10
		suspended 4th		sus4		0, 5, 7
		suspended 2nd		sus2		0, 2, 7
		6th (major)			maj6		0, 4, 7, 9
		minor 6th			min6		0, 3, 7, 9
		9th (dominant)		dom9		0, 4, 7, 10, 14
		major 9th			maj9		0, 4, 7, 11, 14
		minor 9th			min9		0, 3, 7, 10, 14
		diminished 7th		dim7		0, 3, 6, 9
		add9				add9		0, 4, 7, 14
		minor 11th			min11		0, 7, 10, 14, 15, 17
		11th (dominant)		dom11		0, 7, 10, 14, 17
		13th(dominant)		dom13		0, 7, 10, 14, 16, 21
		minor 13th			min13		0, 7, 10, 14, 15, 21
		major 13th			maj13		0, 7, 11, 14, 16, 21
		7-5 (dominant)		dom7-5		0, 4, 6, 10
		7+5 (dominant)		dom7+5		0, 4, 8, 10
		major 7-5			maj7-5		0, 4, 6, 11
		major 7+5			maj7+5		0, 4, 8, 11
		minor major 7		minmaj7 	0, 3, 7, 11
		7-5-9 (dominant)	dom7-5-9	0, 4, 6, 10, 13
		7-5+9 (dominant)	dom7-5+9	0, 4, 6, 10, 15
		7+5-9 (dominant)	dom7+5-9	0, 4, 8, 10, 13
		7+5+9 (dominant)	dom7+5+9	0, 4, 8, 10, 15
	*/

		R.GENRE_MAP = {
			'jazz'		: ['7','maj7','m7'],
			'metal'		: ['maj', 'm', '7', 'm7', '5'],
			'reggae'	: ['maj', 'm', '7', 'm7', '5','maj7', 'add9', 'sus4', 'sus2','m7-5'],
			'latino'	: ['maj','m', '7', 'm7', 'maj7', '5', '6', '9', 'm6'],
			'pop'		: ['maj','m', '7','m7', 'maj7', '5', '6','sus4', 'add9', 'sus2']
		};
		R.GENRES = Object.keys(R.GENRE_MAP);
		R.associatedGenresFor = function(chordType) {
			var a = [];
			R.GENRES.forEach(function(e){
				if( R.GENRE_MAP[e].indexOf(chordType) !== -1 )
					a.push(e);
			});
			return a;
		};

	}

	if (typeof module !== 'undefined' && module.exports) {
		module.exports = bindTo;
	} else {
		var R = this.window.R = this.window.R || {};
		bindTo(R);
	}

}());