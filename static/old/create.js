var sqlite3 = require('sqlite3').verbose();
var DATABASE_FILE = __dirname+'/rechorded.sqlite3';
var async = require('async');
var fs = require('fs');

var R = {};
require('./constants.js')(R);
require('./func.js')(R);
require('./rechorded.js')(R);

if( fs.existsSync(DATABASE_FILE) ) {
	fs.unlinkSync(DATABASE_FILE);
}

var db = new sqlite3.Database(DATABASE_FILE);

db.serialize(function() {
	db.run('create table note(num smallint primary key);');
	db.run('create table genre(name varchar(16) primary key);');

	db.run('create table chordtype(name varchar(16) primary key);');
	db.run('create table chordstep(num smallint,' +
		'type varchar(16) references chordtype(name),primary key (num, type));');
	db.run('create table chord(type varchar(16) not null references chordtype(name),' +
		'root smallint references note(num),primary key (type, root));');

	db.run('create table scaletype(name varchar(16) primary key);');
	db.run('create table scalestep(num smallint,' +
		'scaletype varchar(16) references scaletype(name),primary key (num, scaletype));');
	db.run('create table scale(type varchar(16) not null references scaletype(name),' +
		'root smallint references note(num),' +
		'primary key (type, root));');

	db.run('create table scale2chord(chordtype varchar(16) not null references chord(type),' +
		'chordroot smallint references chord(root),' +
		'scaletype varchar(16) not null references scale(type),' +
		'scaleroot smallint references scale(root),' +
		'primary key (scaletype,scaleroot,chordtype,chordroot));');
	db.run('create table scale2genre(genre varchar(16) not null references genre(name),' +
		'scaletype varchar(16) not null references scale(name),' +
		'scaleroot smallint not null references scale(root),' +
		'primary key (scaletype,scaleroot,genre));');

	db.run('create table tuning(name varchar(32) primary key);');
	db.run('create table tuningmapping(name varchar(32) not null references tuning(name),note int,stringnumber int,primary key(name,stringnumber,note));');

	db.run('create table guitarchord(' +
		'root smallint references chord(root),' +
		'type varchar(16) not null references chord(type),' +
		'barre smallint,' +
		'tuning varchar(32) not null references tuning(name),' +
		'fingers varchar(6) not null,' +
		'difficulty smallint,' +
		'count smallint,' +
		'closed_strings smallint,' +
		'playability int,' +
		'primary key (root,type,barre,tuning,fingers));');

});

db.serialize(function() {
	for(var i=0; i < (12*10); i++ )
		db.run('insert into note values(?)',i);

	R.CHORDS.forEach(function(chordType,j){
		db.run('insert into chordtype values(?)',chordType);
		R.CHORD_MAP[chordType].forEach(function(step,k){
			db.run('insert into chordstep values(?,?)',step,chordType);
		});
	});
	R.NOTES.forEach(function(note,i){
		R.CHORDS.forEach(function(chordType,j){
			db.run('insert into chord values(?,?)',chordType,i);
		});
	});

	R.SCALES.forEach(function(scale,j){
		db.run('insert into scaletype values(?)',scale);
		R.SCALE_MAP[scale].forEach(function(step,k){
			db.run('insert into scalestep values(?,?)',step,scale);
		});
	});
	R.NOTES.forEach(function(note,i){
		R.SCALES.forEach(function(scale,j){
			db.run('insert into scale values(?,?)',scale,i);
		});
	});

	R.GENRES.forEach(function(genre){
		db.run('insert into genre values(?)',genre);
	});
	R.TUNINGS.forEach(function(tuningName){
		db.run('insert into tuning values(?)',tuningName);
		R.TUNING_MAP[tuningName].forEach(function(n,index){
			db.run('insert into tuningmapping values(?,?,?)',tuningName,index,n);
		});
	});

	R.CHORDS.forEach(function(chordtype,m){
		R.NOTES.forEach(function(chordroot,n){
			var c = new R.Chord(chordroot,chordtype);
			var a = c.scales();
			a.forEach(function(pair){
				var root = pair[0],
					type = pair[1];
				db.run('insert into scale2chord values(?,?,?,?)',chordtype,n,R.SCALES[type],root);
			});
		});
	});

});

var store = new R.Store();
store.findAll();
store.all.forEach(function(c){

	var root = 		c.root,
		type = 		R.CHORDS[c.type],
		fingers = 	c.s,
		tuningName = c.tuningName,
		count = 	c.fingersUsed(),
		difficulty = c.difficulty(),
		barre = 	c.barre,
		closed_strings = c.closedCount(),
		playability = c.playability();

	db.run('insert into guitarchord values($root,$type,$barre,$tuning,$fingers,$diff,$count,$closed_strings,$playability);',{
		$root:root,
		$type:type,
		$barre:barre,
		$tuning:tuningName,
		$fingers:fingers,
		$diff:difficulty,
		$count:count,
		$closed_strings:closed_strings,
		$playability:playability
	});
});


db.close();