var R = {};
require('./../src/constants.js')(R);
require('./../src/func.js')(R);
require('./../src/rechorded.js')(R);

var Note 		= R.Note;
var Chord 		= R.Chord;
var GuitarChord = R.GuitarChord;
var Fretboard 	= R.Fretboard;
var Store		= R.Store;

var assert = require("assert");

describe('Note', function(){
	describe('difference',function(){
		it('should 1 between B and C ',function(){
			assert.equal(1, R.difference(11,0));
		});
		it('should 5 between C and G',function(){
			assert.equal(1, R.difference(11,0));
		});
	});
	describe('transpose',function(){
		it('should be B',function(){
			assert.equal(11, R.transposeIgnoreOctave(0,-1));
			assert.equal(11, R.transposeIgnoreOctave(12,-1));
			assert.equal(11, R.transposeIgnoreOctave(13,-2));
			assert.equal(11, R.transposeIgnoreOctave(24,-13));
			assert.equal(11, R.transposeIgnoreOctave(10,13));
		});
		it('should be C',function(){
			assert.equal(0, R.transposeIgnoreOctave(11,1));
			assert.equal(0, R.transposeIgnoreOctave(11,13));
		});
	});
});

describe('possibilities', function(){
	it('should [[1,2],[3,4]] be 4',function(){
		var a = R.possibilities([[1,2],[3,4]]);
		assert.equal(4,a.length);
		console.log(a);
	});
	it('should [[a,b],[0],[1,2]] be 4',function(){
		var a = R.possibilities([['a','b'],[0],[1,2]]);
		assert.equal(4,a.length);
		console.log(a);
	});
	it('not take too long for [[0,1,2,3],[0,1,2,3],[0,1,2,3],[0,1,2,3],[0,1,2,3],[0,1,2,3]]',function(){
		console.time('possibilities performance');
		var a = R.possibilities([[0,1,2,3],[0,1,2,3],[0,1,2,3],[0,1,2,3],[0,1,2,3],[0,1,2,3]]);
		console.timeEnd('possibilities performance');
		console.log('length:'+a.length);
	});
});

describe('permutations', function(){
	it('should [1] for [1]',function(){
		assert.equal(1,R.permutations([1]).length);
	});
	it('should contain 2 entries for [1,2]',function(){
		assert.equal(2,R.permutations([1,2]).length);
		console.log(R.permutations([1,2]));
	});
	it('should contain 6 entries for [1,2,3]',function(){
		assert.equal(6,R.permutations([1,2,3]).length);
		console.log(R.permutations([1,2,3]));
	});
	it('should contain 6 entries for [X,O,3]',function(){
		assert.equal(6,R.permutations(['X','O',3]).length);
		console.log(R.permutations(['X','O',3]));
	});
});

describe('Fretboard', function(){
	describe('positions',function(){
		it('should return [[0,3],[0]] to look for [4,7]',function(){
			var a = new Fretboard([4]).positions([4,7],0);
			assert.ok(a[0].indexOf(R.SYMBOL_OPEN) !== -1);
			assert.ok(a[0].indexOf(R.SYMBOL_CLOSED) !== -1);
			assert.ok(a[0].indexOf(3) !== -1);
			console.log(a);
		});
	});
	describe('find',function(){
		function findTimed(chordString,notes){
			console.time(chordString);
			var a = new Fretboard().find(notes);
			console.timeEnd(chordString);
			return a;
		}
		it('Cmaj',function(){
			var a = findTimed('Cmaj',[0,4,7]);
			assert.ok(a[0].indexOf('X32F1F') !== -1);
			assert.ok(a[3].indexOf('X02220') !== -1);
		});
		it('D',function(){
			var a = findTimed('D',[2,6,9]);
			assert.ok(a[0].indexOf('XXF232') !== -1);
			assert.ok(a[5].indexOf('X02220') !== -1);
		});
		it('F',function(){
			var a = findTimed('F',[5,9,0]);
			assert.ok(a[0].indexOf('133211') !== -1);
			assert.ok(a[1].indexOf('022100') !== -1);
		});
		it('B7',function(){
			var a = findTimed('B7',[11,3,6,9]);
			assert.ok(a[0].indexOf('X212F2') !== -1);
			assert.ok(a[2].indexOf('X02020') !== -1);
		});
		it('all',function(){
			console.time('all');
			//for(var i=0; i < 12; i++) {
				for(var j=0; j < R.CHORDS.length; j++) {
					//console.time(R.CHORDS[j]);
					var a = new Fretboard().find(R.CHORD_MAP[R.CHORDS[j]]);
					//console.timeEnd(R.CHORDS[j]);
				}
			//}
			console.timeEnd('all');
		});
	});
});

describe('Chord', function(){
	it('notesIgnoreOctave',function(){
		var c = new Chord('A','m');
		assert.deepEqual([9,0,4], c.notesIgnoreOctave());
	});
	it('noteMatch',function(){
		var c = new Chord('A','m');
		assert.ok(c.noteMatch([9,0,4]));
		assert.ok(c.noteMatch([9,0,4,0,4,9]));
		assert.ok(!c.noteMatch([0,4]));
		assert.ok(!c.noteMatch([0,4,7,9]));
	});
	it('scales',function(){
		var c = new Chord('A','m');
		var scales = c.scales();
	});
	it("ctor",function(){
		var a = [['C','maj'],[0, R.CHORDS.indexOf('maj')]];
		for( var i=0; i < a.length; i++) {
			var c = new Chord(a[i][0],a[i][1]);
			assert.ok(c.noteMatch([0,4,7]));
			assert.equal(3, c.noteCount());
			assert.ok(c.hasNote(0));
			assert.ok(c.hasNote(4));
			assert.ok(c.hasNote(7));
			assert.ok(!c.hasNote(11));
			assert.ok(c.hasNote(12));
			assert.ok(!c.hasNote(13));
			assert.ok(!c.hasNote(-1));
		}
	});
});

describe('GuitarChord', function(){
	describe('fingersUsed', function(){
		it('with null constructor', function(){
			assert.equal(0,new GuitarChord(0,0,0,null).fingersUsed());
		});
		it('should be 4 with barred Gmaj constructor', function(){
			assert.equal(4,new GuitarChord('G','maj',3,'021100').fingersUsed());
		});
	});
	describe('isValid', function(){
		it('Gmaj', function(){
			assert.ok(new GuitarChord('G','maj',3,'022100').isValid());
			assert.ok(!new GuitarChord('G','maj',3,'122100').isValid());
			assert.ok(!new GuitarChord('G','maj',3,'X22100').isValid());
		});
	});
	describe('isStrummable', function(){
		it('should be true with barred Gmaj constructor', function(){
			assert.ok(new GuitarChord('G','maj',3,'022100').isStrummable());
		});
		it('should be true with barred Dm constructor', function(){
			assert.ok(new GuitarChord('D','m',5,'X03220').isStrummable());
		});
	});
	describe('lowestOpenNote', function(){
		it('should be D with Dm', function(){
			assert.equal(2, new GuitarChord('D','m',5,'X03220').lowestOpenNote(R.DEFAULT_TUNING));
		});
		it('should be G with Gmaj', function(){
			assert.equal(7, new GuitarChord('G','maj',3,'022100').lowestOpenNote(R.DEFAULT_TUNING));
		});
	});
});



describe('Store', function(){
	it('findAll',function(){
			var store = new Store();
			console.time('findAll');
			store.findAll();
			console.timeEnd('findAll');
	});
});


