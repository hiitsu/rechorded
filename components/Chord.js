import React from "react";

export default function Chord(props) {
  const className = 'Chord ' + (props.selected ? ' selected' : '');
  return (
    <div className={className} onClick={() => props.onClick(props.value)}>
      {props.value}
    </div>
  )

}