import React from 'react'
import PropTypes from 'prop-types';

import Link from 'next/link'
import { Container, Row, Col, setConfiguration } from 'react-grid-system';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

class CookieConsent extends React.Component {

  constructor(props) {
    super(props);

    this.handleApprove = this.handleApprove.bind(this);

    this.state = {
      show: false
    }
  }

  componentDidMount(){
    const show = !cookies.get('cookie-consent');
    this.setState({show});
  }

  handleApprove(){
    this.setState({show:false});
    cookies.set('cookie-consent','1', { path: '/' });
  }

  render() {
    if( !this.state.show )
      return (null);

    return <div className="CookieConsent">
      <Container fluid style={{maxWidth: 1024}}>
        <p>Unnatural powers are following you. Mainly Google Analytics, just to see if this content interests anybody.<button onClick={this.handleApprove}>Got it</button></p>
      </Container>
    </div>
  }
}

export default CookieConsent;