import React from "react";

function toLiteral(num) {
  switch(num){
    case 0:
    case 1:
      return 'very easy';
    case 2: return 'easy';
    case 3: return 'moderate';
    case 4: return 'advanced';
    default:
    case 5:
      return 'guru';
  }
}
export default function Difficulty(props) {
  const className = 'Difficulty ' + (props.selected ? ' selected' : '');
  return (
    <div className={className} onClick={() => props.onClick(props.value)}>
      {toLiteral(props.value)}
    </div>
  )

}