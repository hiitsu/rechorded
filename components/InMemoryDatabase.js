import axios from "axios";
import * as R from '../build/R.mjs'

const sortBy = (key) => {
  return (a, b) => (a[key] > b[key]) ? 1 : ((b[key] > a[key]) ? -1 : 0);
};

export default class InMemoryDatabase {

  constructor(){

  }

  initialize(){
    return axios.get('/static/chords.json')
      .then(res => {
        this.db = res.data;
        return res.data;
      });
  }

  beginnerChords(difficulty = 2, tuning = R.DEFAULT_TUNING) {
    const list = this.db.filter( chord => chord.tuning == tuning && chord.difficulty == difficulty );
    list.sort(sortBy('playability'));
    return list;
  }

  variationsOfChord(root = 0, type = 0, tuning = R.DEFAULT_TUNING) {
    const list = this.db.filter( chord => chord.tuning == tuning && chord.root == root && chord.type == type );
    list.sort(sortBy('difficulty'));
    return list;
  }

}
