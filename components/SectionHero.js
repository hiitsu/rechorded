import React from 'react'

import Link from 'next/link'
import { Container, Row, Col, setConfiguration } from 'react-grid-system';
import * as R from '../build/R.mjs'
import Note from "./Note";
import Chord from "./Chord";

function SectionHero(props) {
  return (
    <section className="SectionHero">
      <Container fluid style={{maxWidth:1024}}>
        <Row style={{marginTop:30}}>
          <Col xs={12} sm={4} style={{zIndex:1,overflow:'visible',paddingBottom:10}}>
            <h1 style={{paddingTop:20}}>rechorded is all about chords</h1>
          </Col>
          <Col xs={12} sm={8} style={{zIndex:1,overflow:'visible',paddingBottom:10,paddingTop:15}}>
            {R.NOTES.map((note,index) => <Note key={index} value={note} />)}
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={4} md={5} push={{xs:0, sm:8,md:7}} style={{paddingBottom:10}}>
            <h2 style={{paddingTop:30}}>See how chords map to each instrument</h2>
          </Col>
          <Col xs={12} sm={8} md={7} pull={{xs:0,sm:4,md:5}} style={{paddingBottom:10}}>
            {R.CHORDS.map((chord,index) => <Chord key={index} value={chord} />)}
          </Col>
        </Row>
      </Container>
      <style jsx>{`
        .SectionHero {
          background-color: #fff;
          color:#ccc;
          overflow:hidden;
          padding-bottom:100px;
        }
        h1, h2 {
          letter-spacing: 9px;
          letter-spacing: -2px;
          font-weight: 400;
          /*text-shadow: 1px 1px 7px rgba(0, 0, 0, 0.22);*/
          font-size: 1.6em;
          color:#555;
          margin:0;
        }
        p {
          margin-top: 50px;
          font-size: 30px;
          color: #333;
          font-weight: normal;
          z-index: 2;
        }
        .hero-wrapper {
          position:relative;
          padding-bottom:50px;
        }
        .logo {
          height:60%;
          position:absolute;
          right:10%;
          top:20%;
          z-index: -1;
          user-select: none;
        }
        @media screen and (min-width: 480px) {
          .hero-wrapper {

          }
        }
        .call-to-action {
          height: 48px;
          line-height: 48px;
          padding:0 24px;
          display: inline-block;
          color: white;
          background: #8e8e8e;
          text-align: center;
          border-radius: 32px;
          text-decoration: none;
          margin-top:40px;
        }
      `}</style>
    </section>
  );
}

export default SectionHero;