import React, { Component } from 'react';
import PropTypes from 'prop-types'

import { Container, Row, Col, setConfiguration } from 'react-grid-system';
import * as R from '../build/R.mjs'
import Note from "./Note";
import Instrument from "./Instrument";
import Chord from "./Chord";
import FingerCount from "./FingerCount";
import Difficulty from "./Difficulty";
import {Spinner} from "./Spinner";
import GuitarResultSet from "./GuitarResultSet";

class PageGuitarBeginner extends Component {

  constructor(props) {
    super(props);

    this.handleClickNote = this.handleClickNote.bind(this);
    this.handleClickChord = this.handleClickChord.bind(this);
    this.handleClickFingerCount = this.handleClickFingerCount.bind(this);
    this.handleClickDifficulty = this.handleClickDifficulty.bind(this);
    this.handleClickStrummable = this.handleClickStrummable.bind(this);

    this.state = {
      fingerCount:2,
      difficulty:2,
      tuning:'standard',
      strummable:true,
      variations:[]
    }
  }

  handleClickNote(note){
    this.setState({note});
  }

  handleClickChord(chord){
    this.setState({chord});
  }

  handleClickDifficulty(difficulty){
    this.handleUpdate({difficulty});
  }

  handleClickFingerCount(fingerCount){
    this.handleUpdate({fingerCount});
  }

  handleClickStrummable(strummable){
    this.setState({strummable});
  }

  async handleUpdate(updateObject = {}) {
    const { tuning, root, chord, fingerCount, difficulty } = { ...this.state, ...updateObject };
    //console.time('PageGuitarChordVarations::handleUpdate')
    const variations = this.props.db ?
      this.props.db.beginnerChords(difficulty,tuning) :
      [];
    //console.timeEnd('PageGuitarChordVarations::handleUpdate')
    this.setState({ ...this.state, ...updateObject, variations });
  }

  componentDidUpdate(prevProps, prevState){
    if( prevProps.db == null && this.props.db != null )
      this.handleUpdate()
  }

  componentDidMount() {
    this.handleUpdate();
  }

  render() {
    return <section className="PageBeginner" id="beginner">
      <Container fluid style={{maxWidth:1024}}>
        <Row>
          <Col xs={12}>
            <h1>Beginner chords</h1>
            <p>Here some easy chords that are easy to strum down and doesn't require many fingers or barre grab or reaching fingers wide apart</p>
            <div className="difficulty-container">
              <label ><i className="fa fa-cog"/> Maximum Difficulty</label>
              {[1,2,3,4,5].map((value,index) => <Difficulty key={index} value={value} selected={value == this.state.difficulty} onClick={this.handleClickDifficulty} />)}
            </div>
          </Col>
        </Row>
        <GuitarResultSet variations={this.state.variations} loading={!this.props.db} showChordLabel={true} />
      </Container>
      <style jsx>{`
        .PageBeginner {
          padding-bottom:50px;
          padding-top:20px;
          min-height:calc(100% - 200px);
        }
        label {
          margin-top: 0;
          display:inline-block;
          margin-right:10px;
          margin-bottom:10px;
        }
        label i {
          color:#000;
        }
        .note-container, .chord-container {
          margin-bottom:20px;
        }
      `}</style>
    </section>
  }

}


PageGuitarBeginner.propTypes = {

};

export default PageGuitarBeginner;