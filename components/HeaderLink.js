import { withRouter } from 'next/router'
import Link from "next/link";
import React from "react";

const HeaderLink = (props) => {
  const isActive = props.router.pathname === props.href;
  const className = (props.extraClass || '') + ' HeaderLink ' + (isActive ? 'active' :'');
  return (
    <React.Fragment>
      <Link href={props.href}>
        <a className={className} style={props.style}>
          {props.icon}
          &nbsp;
          <span className="text">{props.children}</span>
        </a>
      </Link>
      <style jsx global>{`
        .links {
          white-space:nowrap;
        }
        .HeaderLink {
          color:white;
          text-align:center;
          display:inline-block;
          margin:0;
          text-decoration:none;
          font-size: 15px;
          line-height: 64px;
          font-weight: 400;
          text-shadow: 1px 1px #00000052;
          user-select:none;
          background-repeat: no-repeat;
          background-position: 8px 50%;
          background-size: 28px 28px;
          padding:0 0 0 38px;
        }
        .HeaderLink.active {
          font-weight:bold;
        }
        .HeaderLink.guitar {
          background-image:url(/static/guitar-only.png);
        }
        .HeaderLink.piano {
          background-image:url(/static/piano-only.png);
        }
        .HeaderLink.piano-and-guitar {
          background-image:url(/static/piano-and-guitar.png);
        }
        .HeaderLink.guitar-beginner {
          background-image:url(/static/guitar-beginner.png);
        }
        .HeaderLink.info {
          padding: 0 0 0 0px;
        }

        a {
          color:white;
          text-align:center;
          display:inline-block;
          margin:0;
          text-decoration:none;
          font-size: 15px;
          line-height: 64px;
          font-weight: 400;
          text-shadow: 1px 1px #00000052;
          user-select:none;
        }
        @media screen and (max-width: 480px) {
          .links {
            margin-left:-10px;
          }
          .HeaderLink {
            /*background-position: 8px 50%;
            background-size: 15px 15px;
            padding:0 0 0 24px;
            min-width:unset;*/
          }
          .text {
            display:none;
            letter-spacing:-1px;
            font-size: 13px;
          }
          .HeaderLink {
            float:left;
            width: 64px;
            margin: 0;
            display: inline-block;
            padding: 0;
            background-position:center center;
          }
          .HeaderLink.active {
            background-color: rgba(255, 255, 255, 0.1);
          }
        }
      `}</style>
    </React.Fragment>
  )
};

export default withRouter(HeaderLink)