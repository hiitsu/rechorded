import React from "react";

export default function Note(props) {
  const className = 'Note ' + (props.selected ? ' selected' : '');
  return (
    <div className={className} onClick={() => props.onClick(props.value)}>
      {props.value}
    </div>
  )

}