import { Component, createElement } from "react"
import * as prodLytics from "./analytics/prodLytics"
import * as devLytics from "./analytics/devLytics"

function isLocal(host) {
  return location.hostname === host;
}

function isDev() {
  return process.env.NODE_ENV !== "production";
}

export default (code, Router, { localhost = "localhost" } = {}) => Page => {
  class WithAnalytics extends Component {
    componentDidMount() {

      const shouldNotTrack = isLocal(localhost) || isDev();
      this.analytics = shouldNotTrack ? devLytics : prodLytics;

      this.analytics.init(code);
      this.analytics.pageview();

      const previousCallback = Router.onRouteChangeComplete;
      Router.onRouteChangeComplete = () => {
        if (typeof previousCallback === "function") {
          previousCallback();
        }
        this.analytics.pageview();
      }
    }

    render() {
      const props = { ...this.props, analytics: this.analytics };
      return createElement(Page,props);
    }
  }

  if (Page.getInitialProps) {
    WithAnalytics.getInitialProps = Page.getInitialProps;
  }

  return WithAnalytics;
};
