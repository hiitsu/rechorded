import React from 'react'
import Link from 'next/link'
import { Container, Row, Col, setConfiguration } from 'react-grid-system';

function Footer(props) {
  return (
    <footer>
      <Container fluid style={{maxWidth:1024}}>
        <Row>
          <Col xs={12}>
            <p className="copyright">
              <span>Jarkko Hiitoluoma © { new Date().getFullYear() }</span>
              <Link href="https://www.twitter.com/jhiitoluoma"><a><i className="fa fa-twitter"></i></a></Link>
              <Link href="https://www.instagram.com/h11tsu/"><a><i className="fa fa-instagram"></i></a></Link>
              <Link href="https://www.linkedin.com/in/jarkko-hiitoluoma"><a><i className="fa fa-linkedin"></i></a></Link>
            </p>
          </Col>
        </Row>
      </Container>
      <style jsx global>{`
        footer {
          padding:3em 0 5em 0;
          background-color:#000;
          color:white;
        }
        footer a {
          text-decoration:none;
          color:white;
          font-size:1em;
        }
        footer a:hover {
          text-decoration:underline;
        }
        .fa {
          margin:0 5px;
          padding:0;
          text-align:center;
          padding: 1em 0;
          color: #eee;
        }
        footer .copyright {
          padding: 1em 0;
          text-align: center;
          color: #ddd;
          font-size: 0.9em;
        }
    `}</style>
    </footer>
  );
}

export default Footer;