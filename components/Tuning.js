import React from "react";

export default function Tuning(props) {
  const className = 'Tuning ' + (props.selected ? ' selected' : '');
  return (
    <div className={className} onClick={() => props.onClick(props.value)}>
      {props.value}
    </div>
  )

}