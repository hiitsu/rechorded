import React from 'react'

import Link from 'next/link'
import { Container, Row, Col, setConfiguration } from 'react-grid-system';

function SectionSummary(props) {
  return (
    <section className="SectionSummary">
      <Container fluid style={{maxWidth:1024}}>
        <Row>
          <Col xs={12}>
            <i className="fa fa-music" />
            <p>Hopefully helps beginners, intermediates and advanced musicians learn new ways play shit.</p>
          </Col>
        </Row>
      </Container>
      <style jsx>{`
        .SectionSummary {
          padding:2em 0 4em 0;
          background: #585858;
          color: black;
        }
        .SectionSummary .fa-music {
          position: absolute;
          left: 18px;
          top: 33px;
          font-size: 106px;
          color:#ddd;
          margin:0;
          padding:0;
        }
        .SectionSummary p {
          margin: 60px 0 60px 150px;
          font-size: 24px;
          color: #ddd;
          font-weight: 300;
          letter-spacing: -0.5px;
          font-family: sans-serif;
        }
      `}</style>
    </section>
  );
}

export default SectionSummary;