import React from 'react'

import Link from 'next/link'

import { Container, Row, Col, setConfiguration } from 'react-grid-system'

function SectionFeatures(props) {
  return (
    <section className="SectionFeatures" id="features">
      <Container fluid style={{maxWidth:1024}}>
        <Row>
          <Col xs={12} sm={12} md={12} style={{position:'relative',paddingBottom:10}}>
            <h2><i className="fa fa-cog" />Features</h2>
            <p>Yet another chord learning and exploration tool. Everybody has unique way to approach music and its theory. This website is a quick port/refactor of this old <a href="https://play.google.com/store/apps/details?id=com.rechorded">application</a> in <a href="https://play.google.com/store">play store</a> that was created for personal reference app/tool years ago.</p>
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={12} md={6}>
            <div className="bullet">
              <h3>+18k chords precalculated</h3>
              <p>Thousands of chords looked up by using naive brute force algorithm.</p>
            </div>
          </Col>
          <Col xs={12} sm={12} md={6}>
            <div className="bullet">
              <h3>Difficulty</h3>
              <p>Chords can be searched and sorted by difficulty, by how many fingers are needed or by algorithm that takes into account if it needs barre grip and and spread the fingers need to be.</p>
            </div>
          </Col>
          <Col xs={12} sm={12} md={6}>
            <div className="bullet">
              <h3>Piano and Guitar</h3>
              <p>There are convenience views for both intsruments for people who are new to either instrument and do not have background in musical theory.</p>
            </div>
          </Col>
          <Col xs={12} sm={12} md={6}>
            <div className="bullet">
              <h3>Variations</h3>
              <p>View all different variations of particular guitar chord in tuning of your choice.</p>
            </div>
          </Col>
          <Col xs={12} sm={12} md={6}>
            <div className="bullet">
              <h3>Multiple tunings</h3>
              <p>Chords are precalculated using naive brute force algorithm in few popular tunings. If you feel there should be another, dont hesitate to ask.</p>
            </div>
          </Col>
          <Col xs={12} sm={12} md={6}>
            <div className="bullet">
              <h3>Scales and chords</h3>
              <p>A lot of convenience views view see how scales and chords are formed in either piano or guitar.</p>
            </div>
          </Col>
        </Row>
      </Container>
      <style jsx>{`
        .SectionFeatures {
          padding: 2em 0 4em 0;
          background: #fa6566;
          color:#fff;
        }
        a {
          font-weight:bold;
          text-decoration:underline;
          line-height: unset;
        }
        .SectionFeatures h2 {
          margin-bottom:.5em;
        }
        .SectionFeatures h2 span {
          position:relative;
        }
        .SectionFeatures .bullet {
          position:relative;
        }

      `}</style>
    </section>
  );
}

export default SectionFeatures;