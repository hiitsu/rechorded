import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { Guitar, Piano } from './visual.js'
import {Canvas} from "./Canvas";

class Instrument extends Component {

  constructor(props) {
    super(props);

    this.paint = this.paint.bind(this);

    this.instrument = this.props.instrument == 'piano' ?
      new Piano() : new Guitar();
  }

  render(){
    return <div className="Instrument">
      <Canvas paint={this.paint} />
      <style jsx>{`
        .Instrument {
          position:relative;
          width:100%;
          height:240px;
          overflow:hidden;
          border:none;
          outline:none;
          margin-bottom:20px;
        }
        canvas {
          outline:none;
          overflow:hidden;
        }
      `}</style>
    </div>
  }


  paint(ctx) {
    this.instrument.setTuning && this.instrument.setTuning(this.props.tuning);
    this.instrument.setChord(this.props.note,this.props.chord,this.props.barre,this.props.strings);
    this.instrument.draw(ctx);
  }
}

Instrument.propTypes = {

};

export default Instrument;