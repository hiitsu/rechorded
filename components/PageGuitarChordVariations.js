import React, { Component } from 'react';
import PropTypes from 'prop-types'

import { Container, Row, Col, setConfiguration } from 'react-grid-system'
import * as R from '../build/R.mjs'
import Note from "./Note"
import Instrument from "./Instrument"
import Chord from "./Chord"
import Tuning from "./Tuning"
import {Spinner} from "./Spinner";
import GuitarResultSet from "./GuitarResultSet";

class PageGuitarChordVariations extends Component {

  constructor(props) {
    super(props);

    this.handleClickRoot = this.handleClickRoot.bind(this);
    this.handleClickChord = this.handleClickChord.bind(this);
    this.handleClickTuning = this.handleClickTuning.bind(this);

    this.state = {
      root:'C',
      chord:'maj',
      tuning:'standard',
      variations:[],
    }
  }

  handleClickRoot(root){
    this.handleUpdate({root});
  }

  handleClickTuning(tuning){
    this.handleUpdate({tuning});
  }

  handleClickChord(chord){
    this.handleUpdate({chord});
  }

  handleUpdate(updateObject = {}) {
    const { tuning, root, chord } = { ...this.state, ...updateObject };
    const rootNum = R.NOTES.indexOf(root);
    const typeNum = R.CHORDS.indexOf(chord);
    //console.time('PageGuitarChordVarations::handleUpdate')
    const variations = this.props.db ?
      this.props.db.variationsOfChord(rootNum,typeNum,tuning):
      [];
    //console.timeEnd('PageGuitarChordVarations::handleUpdate')
    this.setState({ ...this.state, ...updateObject, variations });
  }

  componentDidUpdate(prevProps, prevState){
    if( prevProps.db == null && this.props.db )
      this.handleUpdate()
  }

  componentDidMount() {
    this.handleUpdate();
  }

  render() {
    return <section className="PageGuitarChordVariations" id="variations">
      <Container fluid style={{maxWidth:1024}}>
        <Row>
          <Col xs={12} sm={12} md={6}>
            <div className="note-container">
              <label ><i className="fa fa-music"/> Root</label>
              {R.NOTES.map((value,index) => <Note key={index} value={value} selected={value == this.state.root} onClick={this.handleClickRoot} />)}
            </div>
            <div className="tuning-container">
              <label ><i className="fa fa-music"/> Tuning</label>
              {R.TUNINGS.map((value,index)  => <Tuning key={index} value={value} selected={value == this.state.tuning} onClick={this.handleClickTuning} />)}
            </div>
          </Col>
          <Col xs={12} sm={12} md={6}>
            <div className="chord-container">
              <label ><i className="fa fa-music"/> Chord</label>
              {R.CHORDS.map((value,index)  => <Chord key={index} value={value} selected={value == this.state.chord} onClick={this.handleClickChord} />)}
            </div>
          </Col>
        </Row>
        <GuitarResultSet variations={this.state.variations} loading={!this.props.db} />
      </Container>
      <style jsx>{`
        .PageGuitarChordVariations {
          padding-bottom:100px;
          padding-top:20px;
        }
        label {
          margin-top: 0;
          display:block;
          margin-bottom:10px;
        }
        label i {
          color:#000;
        }
        .note-container, .chord-container {
          margin-bottom:20px;
        }
      `}</style>
    </section>
  }

}


PageGuitarChordVariations.propTypes = {

};

export default PageGuitarChordVariations;