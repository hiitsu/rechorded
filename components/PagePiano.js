import React, { Component } from 'react';
import PropTypes from 'prop-types'

import { Container, Row, Col, setConfiguration } from 'react-grid-system';
import * as R from '../build/R.mjs'
import Note from "./Note";
import Instrument from "./Instrument";
import Chord from "./Chord";

class PagePiano extends Component {

  constructor(props) {
    super(props);

    this.handleClickNote = this.handleClickNote.bind(this);
    this.handleClickChord = this.handleClickChord.bind(this);

    this.state = {
      note:'C',
      chord:'maj'
    }
  }

  handleClickNote(note){
    this.setState({note});
  }

  handleClickChord(chord){
    this.setState({chord});
  }

  oneTimeInitialization() {
    // TODO init
  }

  render() {
    return <section className="PageExplore" id="explore">
      <Container fluid style={{maxWidth:1024}}>
        <Row>
          <Col xs={12}>
            <div className="note-container">
              <label ><i className="fa fa-music"/> Root</label>
              {R.NOTES.map((note,index) => <Note key={index} value={note} selected={note == this.state.note} onClick={this.handleClickNote} />)}
            </div>
            <div className="chord-container">
              <label ><i className="fa fa-music"/> Chord</label>
              {R.CHORDS.map((chord,index) => <Chord key={index} value={chord} selected={this.state.chord == chord} onClick={this.handleClickChord} />)}
            </div>
            <label>Piano {this.state.note} {this.state.chord} </label>
            <Instrument instrument="piano" note={this.state.note} chord={this.state.chord} />
          </Col>
        </Row>
      </Container>
      <style jsx>{`
        .PageExplore {
          padding-bottom:100px;
          padding-top:20px;
        }
        label {
          margin: 0 20px 10px 0;
          display:inline-block;
        }
        label i {
          color:#000;
        }
        .note-container, .chord-container {
          margin-bottom:20px;
        }
      `}</style>
    </section>
  }

}


PagePiano.propTypes = {

};

export default PagePiano;