import React, { Component } from 'react';
import PropTypes from 'prop-types'

import { Container, Row, Col, setConfiguration } from 'react-grid-system'
import * as R from "../build/R.mjs";
import {Spinner} from "./Spinner";
import Instrument from "./Instrument";

export default function GuitarResultSet({loading,variations,showChordLabel}){
  if( loading ) {
    return <Row>
        <Col xs={12} style={{textAlign:'center',paddingTop:20}}>
          <Spinner width={128} height={128} color={'#cdcdcd'} style={{display:'inline-block'}} />
      </Col>
    </Row>
  }
  return <Row>
    <Col xs={12}>

    </Col>
    {variations.map( (variation,index) => {
      return <Col key={index} xs={12} sm={6} md={6} lg={4}>
        {showChordLabel && <label>{R.NOTES[variation.root]} {R.CHORDS[variation.type]}</label>}
        <Instrument tuning={variation.tuning} instrument="guitar" note={variation.root} chord={variation.chord} barre={variation.barre} strings={variation.s} />
      </Col>
    })}
  </Row>
}