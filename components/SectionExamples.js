import React from 'react'

import Link from 'next/link'

import { Container, Row, Col, setConfiguration } from 'react-grid-system';

function SectionExamples(props) {
  return (
    <section className="SectionExamples" id="examples">
      <Container fluid style={{maxWidth:1024}}>
        <Row>
          <Col xs={12}>
            <h2 >Examples</h2>
            <p className="subtitle">Here are some things...</p>
          </Col>
        </Row>
      </Container>
      <style jsx>{`
        .SectionExamples {
          padding:2em 0 4em 0;
          background-color:#FBCB1B;
          color:black;
        }
        .SectionExamples img {
          width:100%;
        }
        h2, h3 {
          font-weight: 400;
        }
        .title {
          display:inline-block;
          margin:10px 10px 5px 0;
        }
        .subtitle {
          display:inline-block;
          font-size:0.9rem;
          margin:0 0 80px 0;
        }
        .example-wrapper {
          margin:5px;
          position:relative;
          background: #f7f7f7;
          border: 0px solid #ededed;
          box-sizing: border-box;
          box-shadow: 1px 1px 5px 0px #0000002b;
        }
        .info-wrapper p {
          margin:0 0 10px 0;
        }
        .info-wrapper a {
          position: absolute;
          bottom: 10px;
          right: 10px;
          text-decoration: none;
          color: #2296f3;
          font-weight: 400;
        }

        .info-wrapper a:hover {
          text-decoration: underline;
        }
        .example-wrapper .play {
          width: 40px;
          height: 40px;
          text-align: center;
          position: absolute;
          left: calc(50% - 20px);
          top: calc(50% - 20px);
          color: white;
          border-radius: 50%;
          background: rgba(0, 0, 0, 0.58);
          outline:none;
          user-select:none;
          border:none;
        }

        .example-wrapper .play:hover {
          background: #bababa;
        }

        .play .fa {
          margin-left: 4px;
          font-size: 16px;
        }
      `}</style>
    </section>
  );
}

export default SectionExamples;