import React from "react";

export default function FingerCount(props) {
  const className = 'FingerCount ' + (props.selected ? ' selected' : '');
  return (
    <div className={className} onClick={() => props.onClick(props.value)}>
      {props.value}
    </div>
  )

}