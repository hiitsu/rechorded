import React from 'react'
import PropTypes from 'prop-types';

import Link from 'next/link'
import Head from 'next/head'
import {Container, Row, Col, setConfiguration} from 'react-grid-system';
import Footer from "./Footer";
import Header from "./Header";
import CookieConsent from "./CookieConsent";

// <script key="gtm" dangerouslySetInnerHTML={{__html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-123123');`}} />
// <noscript dangerouslySetInnerHTML={{__html: `<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M69664" height="0" width="0" style="display:none;visibility:hidden;"></iframe>`}} />

function Layout(props) {

  setConfiguration({
    breakpoints: [480, 640, 960, 1024],
    containerWidths: [480, 640, 960, 1024],
    gutterWidth: 20
  });

  return <div className="Layout" style={props.style}>

    <Head>
      <meta charSet='utf-8'/>
      <meta key="viewport" name="viewport" content="initial-scale=1.0, width=device-width"/>
      <link key="apple-touch-icon" rel="apple-touch-icon" sizes="180x180" href="/static/apple-touch-icon.png"/>
      <link key="icon-32" rel="icon" type="image/png" sizes="32x32" href="/static/favicon-32x32.png"/>
      <link key="icon-16" rel="icon" type="image/png" sizes="16x16" href="/static/favicon-16x16.png"/>
      <link key="manifest" rel="manifest" href="/static/site.webmanifest"/>
      <link key="mask-icon" rel="mask-icon" href="/static/safari-pinned-tab.svg" color="#5bbad5"/>
      <meta key="msapplication-TileColor" name="msapplication-TileColor" content="#da532c"/>
      <meta key="theme-color" name="theme-color" content="#ffffff"/>
      <link key="fonts" href='//fonts.googleapis.com/css?family=Montserrat:300,400,700' rel='stylesheet'
            type='text/css'/>
      <link key="fa" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
            integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
            crossOrigin="anonymous"/>
    </Head>

    <CookieConsent/>
    <Header/>
    {props.children}
    <noscript>This site needs Javascript enabled to function. Please enable Javascript and reload page.</noscript>
    <Footer/>
    <style jsx global>{`
      html, body {
        height: 100%;
        overflow: hidden;
      }
      body {
        margin:0;
        padding:0;
        overflow:auto;
      }
      body,h1,h2,h3,h4,h5,p {
        font-family: Montserrat,Helvetica Neue,Helvetica,sans-serif,Arial;
        -webkit-font-smoothing:antialiased;
        font-smooth: auto;
      }
      h1,h2,h3,h4,h5,label {
        font-weight:bold;
      }
      p {
        font-weight:normal;
      }
      .Layout {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        overflow-y: auto;
        overflow-x: hidden;
      }
      .CookieConsent {
        position: fixed;
        bottom: 10px;
        right: 10px;
        width: 280px;
        padding: 20px;
        box-sizing: border-box;
        background: #ddd;
        color: black;
        z-index: 100001;
      }
      .CookieConsent p {
        margin:0;
        padding:0;
        line-height: 24px;
        display:inline-block;
      }
      .CookieConsent button {
        display:inline-block;
        margin:0 8px;
        background: #000;
        border: 0;
        color: #fff;
        padding:8px 16px;
      }
      .CookieConsent a {
        color:white;
      }
      .FingerCount {
        background: #ae29d7;
        width: 44px;
        height: 44px;
        margin: 0 8px 8px 0;
        line-height: 44px;
        text-align: center;
        display: inline-block;
        border-radius: 2px;
        box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.22);
        color:#fff;
        box-sizing: border-box;
        user-select:none;
      }
      .FingerCount.selected, .FingerCount:hover {
         background: #ed49d7;
      }
      .Difficulty {
        background: #19bbb2;
        padding: 0 12px;
        height: 44px;
        margin: 0 8px 8px 0;
        line-height: 44px;
        text-align: center;
        display: inline-block;
        border-radius: 2px;
        box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.22);
        color:#fff;
        box-sizing: border-box;
        user-select:none;
      }
      .Difficulty.selected, .Difficulty:hover {
         background: #29ccc2;
      }
     .Tuning {
        background: #8BC34A;
        width: 96px;
        height: 44px;
        margin: 0 8px 8px 0;
        line-height: 44px;
        text-align: center;
        display: inline-block;
        border-radius: 2px;
        box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.22);
        color:#fff;
        box-sizing: border-box;
        user-select:none;
      }
      .Tuning.selected, .Tuning:hover {
         background: #9Bd35A;
      }
     .Note {
        background: #F95B5B;
        width: 44px;
        height: 44px;
        margin: 0 8px 8px 0;
        line-height: 44px;
        text-align: center;
        display: inline-block;
        border-radius: 2px;
        box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.22);
        color:#fff;
        box-sizing: border-box;
        user-select:none;
      }
      .Note.selected, .Note:hover {
         background: #Ff7B7B;
      }

      .Chord {
        background: #FBCB1B;
        height: 44px;
        width: 64px;
        margin: 0 8px 8px 0;
        line-height: 44px;
        text-align: center;
        display: inline-block;
        border-radius: 2px;
        box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.22);
        color:#fff;
        font-size:13px;
        box-sizing: border-box;
        user-select:none;
      }
      .Chord.selected, .Chord:hover {
         background: #eBaB0B;
      }

      .Scale {
        background: #1b80fb;
        height: 44px;
        width: 96px;
        margin: 0 10px 10px 0;
        line-height: 44px;
        text-align: center;
        display: inline-block;
        border-radius: 2px;
        box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.22);
        box-sizing: border-box;
        user-select:none;
      }
    `}</style>
  </div>
}

Layout.propTypes = {
  style: PropTypes.object
};

export default Layout;