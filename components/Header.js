import React from 'react'

import { Container, Row, Col, setConfiguration, Visible, Hidden  } from 'react-grid-system'
import HeaderLink from "./HeaderLink"

// TODO printout view, training view where you take few cords tick
// <HeaderLink href="/guitar-and-piano" extraClass="piano-and-guitar">Guitar & Piano</HeaderLink>
function Header(props) {
  return (
    <header style={props.style || {}}>
      <Container fluid style={{maxWidth:1024,position:'relative'}}>
        <Row>
          <Col xs={12}>
            <div className="links">
              <HeaderLink href="/" extraClass="info" icon={<i className="fa fa-home" />}>About</HeaderLink>
              <HeaderLink href="/piano" extraClass="piano">Piano</HeaderLink>
              <HeaderLink href="/guitar-variations" extraClass="guitar">Guitar</HeaderLink>
              <HeaderLink href="/guitar-beginner" extraClass="guitar-beginner">Beginner</HeaderLink>
            </div>
          </Col>
        </Row>
      </Container>
      <style jsx>{`
        header {
          background: #6742eb;
          height: 64px;
          line-height: 48px;
          color: white;
          overflow: hidden;
        }
      `}</style>
    </header>
  );
}

export default Header;